/**
 * Created by HP on 09-Sep-17.
 */
/**
 * Created by HP on 09-Sep-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);


function helper(line){
    console.log(fibonacciNumberNaive(line));
    process.exit();
}

function fibonacciNumberNaive (n) {
    if (n <= 1) {
        return n;
    }
    return (fibonacciNumberNaive(n - 1) + fibonacciNumberNaive(n - 2));
}