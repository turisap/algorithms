/**
 * Created by HP on 14-Sep-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);

var currentLine = 1;
var segments    = [];
var n           = 0;

function helper(line) {
    if (currentLine === 1) {
       n = parseInt(line)
    }
    if (currentLine > 1) {
        segments.push(line.split(' ').map(Number));
    }
    if (currentLine === n + 1) {
        visitThoseTenants(n, segments);
        rl.close();
        process.exit();
    }
    currentLine++;
}

function visitThoseTenants(n, segments) {
    var coverPoints = [];

    while (segments.length > 0) {
        var firstPoint = findLeftmostSegment(segments);
        removeElements(segments, firstPoint);
        coverPoints.push(firstPoint);
    }

    console.log(coverPoints.length);
    console.log(coverPoints.toString().replace(/,/g, ' '));

}

function findLeftmostSegment(segments) {
    var minRightPoint = (segments[segments.length - 1].slice(-1));


    segments.forEach(function (item) {
        var currentRightPoint = item.slice(-1);

        if (currentRightPoint[0] < minRightPoint[0]) {
            minRightPoint = currentRightPoint;
        }
    });
    return minRightPoint.pop();
}

function removeElements(segmentsArray, leftMostPoint) {
    var indexesToDelete = [];

    segmentsArray.forEach(function (item) {
        var currentBeginning = item[0];
        if (currentBeginning <= leftMostPoint) {
            indexesToDelete.push(segmentsArray.indexOf(item));
        }
    });

    for (var k = (indexesToDelete.length - 1); k >= 0; k--) {
        segmentsArray.splice(indexesToDelete[k], 1);
    }
}


