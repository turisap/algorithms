/**
 * Created by HP on 15-Sep-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

var currentLine = 1;
var n = 0;

rl.on('line', helper);

function helper(line) {

    if (currentLine === 1) {
        n = parseInt(line);
    }
    if (currentLine === 2) {
        var sequence = line.split(' ').map(Number);

        makeMeWellPaidNaive(sequence, n);

        rl.close();
        process.exit();
    }
    currentLine++;
}

function makeMeWellPaidNaive(sequence, n) {
    var currentMax = sequence.toString().replace(/,/g, '');

    for (var i = 0; i < factorial(n * 2); i++) {
        //var clone = sequence.slice(0);
        var indexes = [];

        while (indexes.length < sequence.length) {
            var randomIndex = Math.floor(Math.random() * (sequence.length));
            if(indexes.indexOf(randomIndex) === -1) {
                indexes.push(randomIndex);
            }
        }
        //console.log(indexes);

        var string = '';
        for (var j = 0; j < indexes.length; j++) {
            string += sequence[indexes[j]].toString();
            //string += ' ';
        }

        if (parseInt(string) > parseInt(currentMax)) {
           currentMax = string;
        }

    }
    console.log(currentMax);
}


var factorial = function (n) {
    //console.log(n);
    if (n === 0) {
        return 1;
    }
    return factorial(n - 1) * n;
};

