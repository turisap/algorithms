#python3
import sys
import random
#import itertools

def partition3(a, l, r):
    randomElement1 = random.randrange(l, r)
    randomElement2 = random.randrange(l, r)
    randomElement3 = random.randrange(l, r)
    randomElement4 = random.randrange(l, r)
    randomElement5 = random.randrange(l, r)
    randomList     = [a[randomElement1], a[randomElement2], a[randomElement3], a[randomElement4], a[randomElement5]]
    randomListSorted = sorted(randomList)
    #print randomListSorted
    mediana = randomListSorted[2]
    randomElement = a.index(mediana)
    #print randomElement
    a[randomElement], a[l] = a[l], a[randomElement]
    j = l

    for i in range(l + 1, len(a)):
        
        if a[i] <= a[l]:
            j += 1
            a[j], a[i] = a[i], a[j]
    a[j], a[l] = a[l], a[j]

    
    newPlace = j - 1
    for c in reversed(range(0, j)):
        if a[c] == a[j]:
            a[c], a[newPlace] = a[newPlace], a[c]
            newPlace -= 1
    newPlace += 1
    
    return [newPlace, j]



def hoarePartition(a, l, r):
   
    i = l 
    j = r 
   
    while True:
        if i >= r: break
        #print("i = ({}); a[i] = ({}); a[l] = ({}); j = ({})".format(i, a[i], a[l], j))
        while a[i] <= a[l]:
            i += 1
            if i == r: break
        while a[j] > a[l]:
            j -= 1
            if j == l: break
        if i >= j:
            break
        
        a[i], a[j] = a[j], a[i]
    a[l], a[j] = a[j], a[l]
    return j
    
    

def randomized_quick_sort(a, l, r):

    if l >= r:
        return
    
    if(arrayIsSorted(a, l, r)):
        #print 'dk'
        return a
   
    m = hoarePartition(a, l, r)
    #print list
    randomized_quick_sort(a, l, m - 1);
    randomized_quick_sort(a, m + 1, r);


def arrayIsSorted(a, l, r):
    for i in range(l, r + 1):
        if i == r: break
        if a[i] > a[i + 1]:
            return False
    return True


if __name__ == '__main__':
    list = [7, 2, 15, 41, 6, 5, 9, 0, 17, 32]
    #list  = [2, 1, 7, 3, 9, 6]
    #list  = [2, 3, 9, 2, 9]
    #list   = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
    #print list
    (randomized_quick_sort(list, 0, len(list) - 1))
    print list
   
