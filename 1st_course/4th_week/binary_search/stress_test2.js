/**
 * Created by HP on 25-Sep-17.
 */
var bigInt = require('big-integer');

(function generator() {

    this.randomAscendingIntegers = function () {
        var randomNumber = Math.floor(Math.random() * this.minValue + 1) + this.minValue;
        this.minValue = randomNumber;
        return randomNumber;
    };


    //console.log(searchedValues);
    //console.log(searchableArray);

    while (true) {

        var randomArrayLength = Math.floor(Math.random() * 1000);
        var randomValueLength = Math.floor(Math.random() * 1000);
        var searchableArray  = [];
        var searchedValues   = [];
        this.minValue        = 2;

        for (var i = 1; i < randomArrayLength; i++) {
            var randomArrayValue = this.randomAscendingIntegers();
            searchableArray.push(randomArrayValue);
        }
        for (var c = 0; c < randomValueLength; c++) {
            var randomValueValue = bigInt(Math.floor(Math.random() * 100));
            searchedValues.push(randomValueValue);
        }

        var fastSolution = findValues(searchableArray, searchedValues);
        var naiveSolution = searchNaive(searchableArray, searchedValues);

        if (fastSolution !== naiveSolution) {
            console.log(`\n`);
            console.log(`Array to search: ${searchableArray}`);
            console.log('\n');
            console.log(`Values: ${searchedValues}`);
            console.log('\n');
            console.log(`ERROR!!!! Fast  solution: ${fastSolution}`);
            console.log('\n');
            console.log(`ERROR!!!! Naive Solution: ${naiveSolution}`);
            break;
        }

        console.log(`OK ${fastSolution} == ${naiveSolution}`);
        console.log('\n');

    }


})();

function findValues(array, values){
    var results = [];
    values.forEach(function(item) {
        results.push(binarySearch(array, item));
    });
    return results.join(' ');
}

function targetMoreThanGuess(target, array, guess) {
    return target.compare(array[guess]) === 1;
}
function targetLessThanGuess(target, array, guess) {
    return target.compare(array[guess]) === -1;
}
function targetEqualGuess(target, array, guess) {
    return target.compare(array[guess]) === 0;
}
function binarySearch(array, target) {
    //console.log(array);
    var min = 0;
    var max = array.length - 1;

    while (min <= max) {
        var guess = Math.floor((max + min) / 2);

        if (targetEqualGuess(target, array, guess)) {
            return guess;
        } else if (targetMoreThanGuess(target, array, guess)) {
            min = guess + 1;
        } else if (targetLessThanGuess(target, array, guess)) {
            max = guess - 1;
        }
    }
    return -1;
}

/** ============================ THE NAIVE SOLUTION ====================== **/

function searchNaive(searchArray, searchValues) {
    //console.log(searchArray, searchValues);
    var resultsArray = [];

    for (var i = 0; i < searchValues.length; i++) {
        //console.log(`lsdjflsdjflksjdfl ${searchValues[i]}`)
        resultsArray.push(searchArray.indexOf(parseInt(searchValues[i])));
    }
    return resultsArray.join(' ');
}
