/**
 * Created by HP on 13-Sep-17.
 */
var bigInt = require('big-integer');

while (true) {
    var n = Math.ceil(Math.random() * 10000);
    var m = Math.ceil(Math.random() * 1000);

    console.log(`\n`);
    console.log(`====================NEW ROW=====================`);
    console.log(`N: ${n}, m: ${m}`)

    var fastImplementation = fibonacciModulesFast(n, m);
    var naiveImplementation = fibonacciModuleNaive(n, m);

    if(fastImplementation != naiveImplementation) {
        console.log('\n');
        console.log(`WRONG ANSWER!!!!! N: ${n}, M: ${m}; Fast Implementation = ${fastImplementation}, Naive Implementation
 = ${naiveImplementation}`);
        console.log('\n');
    }

    console.log('\n');
    console.log(`OK. ${fastImplementation} = ${naiveImplementation}`);
}



function fibonacciModuleNaive(n, m) {

    var previous = bigInt(0);
    var current  = bigInt(1);

    for (var i = 0; i < (n - 1); i++) {
        var temporaryPrevious = previous;
        previous  = current;
        current   = current.add(temporaryPrevious);
    }

    return current.mod(m).toString();
}



function fibonacciModulesFast(member, m) {

    if (checkIfFirstOrSecond(parseInt(n.toString()))) {
        return n.toString();
    }

    var lastDigits    = [bigInt(0), bigInt(1)];
    var sequenceFound = false;
    var zeroFound     = false;
    var counter       = 2;

    while (! sequenceFound) {

        var currentLastDigit = (lastDigits[counter - 1].add(lastDigits[counter - 2])).mod(10);
        lastDigits.push(currentLastDigit);

        if (zeroFound && currentLastDigit.toString() == 1) {
            sequenceFound = true;
        }
        if (zeroFound && currentLastDigit.toString() != 1) {
            zeroFound = false;
        }
        if (currentLastDigit.toString() == 0) {
            zeroFound = true;
        }
        counter++;
    }

    lastDigits.pop();
    lastDigits.pop();

    var index = n.mod(lastDigits.length);
    return lastDigits[index].toString();
}

function checkIfFirstOrSecond(n) {
    return (n === 0 || n === 1);
}