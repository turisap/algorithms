/**
 * Created by HP on 01-Oct-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);
var currentLine = 1;
var capacity    = 0;
var barsWeights = [];

function helper(line) {
    if (currentLine === 1) {
        var data = line.trim().split(' ');
        capacity = data.shift();
        //console.log(capacity);
    }
    if (currentLine === 2) {
        barsWeights = line.trim().split(' ');
        barsWeights = barsWeights.map(function(item) {
            return parseInt(item);
        });
        getTonsOfGold(capacity, barsWeights);
        rl.close();
        process.exit();
    }
    currentLine++
}

function getTonsOfGold(W, weights) {
    var resultsTable = [];
    weights.unshift(0);
    var weightsNumber = weights.length;

    this.getFirstRow = function (weight, row) {
        while (weight >= 0) {
            row.push(0);
            weight--;
        }
        return row;
    };


    for (var i = 0; i < weightsNumber; i++) {
        var row = [];
        if (i === 0) {
            resultsTable.push(this.getFirstRow(W, row));
            continue;
        }
        resultsTable.push(row);

        for (var w = 0; w <= W; w++) {

            if (w === 0) {row.push(0); continue;}
            var valueIandW = resultsTable[i - 1][w];
            if (weights[i] <= w) {
                var currentValue = resultsTable[i - 1][w - weights[i]] + weights[i];
            }
            resultsTable[i][w] = (currentValue >= valueIandW) ? currentValue : valueIandW;
            currentValue = 0;
        }
    }
    /*for(var j = 0; j < resultsTable.length; j++) {
        resultsTable[j] = resultsTable[j].join(' ');
    }*/
    console.log((resultsTable.pop()).pop());
}
