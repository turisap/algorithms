/**
 * Created by HP on 14-Sep-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);

function helper(line) {
    var m = parseInt(line.trim());
    console.log(getChange(m));

    rl.close();
    process.exit();
}

function getChange(m) {
    var numberOfCoins = 0;
    var moneyLeft = m;

    while (moneyLeft >= 10) {
        numberOfCoins += 1;
        moneyLeft -= 10;
    }

    if (moneyLeft > 5) {
        numberOfCoins += moneyLeft % 5 + 1;
        moneyLeft = 0;
    }
    if (moneyLeft === 5) {
        numberOfCoins += 1;
        moneyLeft = 0;
    }
    if (moneyLeft < 5) {
        numberOfCoins += moneyLeft;
    }

    return numberOfCoins;
}

