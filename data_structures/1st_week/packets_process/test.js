/**
 * Created by HP on 04-Oct-17.
 */
(function generator(){
    var sequenceLength = 100000;
    var bufferCapacity = 10;
    var packs = [];

    for (var i = 0; i < sequenceLength; i++) {
        var arrivaltime = 0;
        if (i > Math.floor(sequenceLength / 5)) {arrivaltime = 1;}
        if (i > Math.floor(2 * sequenceLength / 5)) {arrivaltime = 2;}
        if (i > Math.floor(3 * sequenceLength / 5)) {arrivaltime = 3;}
        if (i > Math.floor(4 * sequenceLength / 5)) {arrivaltime = 4;}
        if (i > Math.floor(5 * sequenceLength / 5)) {arrivaltime = 5;}
        packs.push([arrivaltime, Math.floor(Math.random() * 1000)]);
    }
    console.log(processor(packs, bufferCapacity));
    //console.log(packs[0]);
    //console.log(packs[20000]);
    //console.log(packs[45000]);
})();


function processor(values, bufferCapacity) {
    var resultingTime = [];

    this.init = function () {
        this.processor(values);
    };

    this.processor = function (packages) {
        var read = 0;
        var write = 0;

        var buffer = new Array(bufferCapacity + 1).fill(-1);
        var finishTime = 0;
        //console.log(buffer)


        for (var i = 0, len1 = packages.length; i < len1; i++) {
            var currentArrival = packages[i][0];
            var currentProcessing = packages[i][1];

            console.log(i);
            cleanBuffer();
            console.log(i + 'k');

            if (currentArrival >= finishTime) { // if package comes after processing a previous one
                resultingTime.push(currentArrival);
                finishTime = currentArrival + currentProcessing;
                if (currentProcessing > 0) {enqueue(finishTime);}
            } else {
                if (bufferCanAccept()) {
                    resultingTime.push(finishTime);
                    finishTime += currentProcessing;
                    enqueue(finishTime);
                } else {
                    resultingTime.push(-1);
                }
            }
        }

        function cleanBuffer() {
            for(var f = 0, len3 = buffer.length; f < len3; f++){
                if (/*! isNaN(buffer[f])*/ buffer[f] !== -1) {
                    if (buffer[f] <= currentArrival) {
                        dequeue()
                    } else {break;}
                }
            }
            /*buffer.forEach(function (item) {
                if (! isNaN(item)) {
                    if (item <= currentArrival) {dequeue()}
                }
            })*/
        }

        function bufferCanAccept(){
            var freeSpaces = 0;
            for (var d = 0, len2 = buffer.length; d < len2; d++){
                if(/*isNaN(buffer[d])*/buffer[d] === -1){freeSpaces++}
                if(freeSpaces > 1) {break;}
            }
            //console.log(`read = ${read}, write = ${write}, buffer = ${buffer} bufferCanAccept = ${freeSpaces > 1}`)
            return freeSpaces > 1;
        }
        function enqueue(element) {
            buffer[write] = element;
            write = (write + 1) % buffer.length;
        }
        function dequeue() {
            var dequeued = buffer[read];
            buffer[read] = 'empty';
            read = (read + 1) % buffer.length;
        }
    };
    /* cd xampp/htdocs/courses/algorithms/data_structures/1st_week/packets_process */
    this.reformatPackets = function (values) {
        var Package = function (valuePair) {
            this.arrivalTime = parseInt(valuePair.shift());
            this.processingTime = parseInt(valuePair.shift());
        };
        var packagesForProcessing = [];

        values.forEach(function(value) {
            packagesForProcessing.push(new Package(value));
        });
        return packagesForProcessing;
    };
    this.init();
    return resultingTime;
}