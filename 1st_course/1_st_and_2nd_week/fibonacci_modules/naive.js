/**
 * Created by HP on 10-Sep-17.
 */
var readline = require('readline');
var bigInt  = require('big-integer');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);


function helper(line){
    var n = parseInt(line.toString().split(' ').shift());
    var m = parseInt(line.toString().split(' ').pop());
    console.log(fibonacciModule(n, m));
    rl.close();
    process.exit();
}

function fibonacciModule(n, m) {

    var previous = bigInt(0);
    var current  = bigInt(1);

    for (var i = 0; i < (n - 1); i++) {
        var temporaryPrevious = previous;
        previous  = current;
        current   = current.add(temporaryPrevious);
    }

    return current.mod(m).toString();
}


