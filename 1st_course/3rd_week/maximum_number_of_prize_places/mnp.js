/**
 * Created by HP on 15-Sep-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);

function helper(line) {
    getMaximumNumberOfPrizePlaces(line);
    rl.close();
    process.exit();
}

function getMaximumNumberOfPrizePlaces(line) {
    var n = parseInt(line);
    var candies = [];
    var places = 1;

    if (n <= 2) {
        console.log(places);
        console.log(n);
        return true;
    }

    var left = n;
    for (var l = 1; l <= n; l++) {

        if (left <= 0) {
            break;
        }

        if (left <= 2 * l) {
            candies.push(left);
            break;
        }
        left -= l;
        candies.push(l);
    }
    console.log(candies.length);
    //console.log(candies.toString().replace(/,/g, ' '));
    candies.forEach(function (item) {
        process.stdout.write(item.toString() + ' ');
    })
}