/**
 * Created by HP on 15-Oct-17.
 */
let readline = require('readline');
let rl = readline.createInterface(process.stdin, process.stdout);
let cl = 1;
let n = 0;
let m = 0;
let list = [];
let path = [];

rl.on('line', assigner);

function assigner(line) {
    if (cl === 1) {
        let data = line.trim().split(' ');
        n = parseInt(data[0]);
        m = parseInt(data[1]);
    }
    if (1 < cl && cl <= m + 1) {
        list.push(line.trim().split(' ').map(Number));
    }
    if (cl === m + 2) {
        path = line.trim().split(' ').map(Number);
    }
    if (cl === m + 2) {
        pathFinder();
        rl.close();
        process.exit();
    }
    cl++;
}

function Graph(vNum, edges) {
    let adjList =  makeAdjacentList();
    let nodes   = createNodes();
    this.explore = function(v) {
        nodes[v - 1].visited = true;
        adjList[v - 1].forEach((item) => {
            if (!nodes[item - 1].visited) this.explore(item);
        });
        return nodes;
    };
    function Node() {
        this.visited = false;
    }
    function createNodes () {
        let res = [];
        for (let i = 0; i < vNum; i++){res.push(new Node);}
        return res;
    }
    function makeAdjacentList() {
        let adjList = [];
        for (let i = 0; i < vNum; i++) {
            adjList.push([])
        }
        for (let c = 0; c < edges.length; c++) {
            adjList[edges[c][0] - 1].push(edges[c][1]);
            adjList[edges[c][1] - 1].push(edges[c][0]);
        }
        return adjList;
    }
}

function pathFinder(){
    let gr = new Graph(n, list);
    let exploredGr = gr.explore(path[0]);
    //console.log(exploredGr);
    console.log(exploredGr[path[1] - 1].visited ? 1 : 0);
}