/**
 * Created by HP on 10-Sep-17.
 */
//var bigInt = require('big-integer');

while (true) {
    var limit = 2000000;
    var intA = Math.ceil(Math.random() * limit);
    var intB = Math.ceil(Math.random() * limit);

    var fastImplementation = leastCommonMultiply(intA, intB);
    var naiveImplementation = leastCommonMultiplyNaive(intA, intB);

    if (fastImplementation !== naiveImplementation) {
        console.log('\n\n');
        console.log(`WRONG ANSWER!!! A: ${intA}, B: ${intB}; FastImplementation: ${fastImplementation}, SlowImplementation: ${naiveImplementation}`);
        console.log('\n\n');
        break;
    }
    console.log('\n');
    console.log(`Both are same: ${naiveImplementation} = ${fastImplementation}`);
    console.log('\n');
}


function leastCommonMultiply(intA, intB) {
    var gcd = greatestCommonDivisor(intA, intB);

    if (intA > intB) {
        return ((intA / gcd) * intB);
    } else {
        return ((intB / gcd) * intA);
    }
}

function greatestCommonDivisor(intA, intB) {
    if (intB === 0) {
        return intA;
    }
    var aPraim = intA % intB;
    return greatestCommonDivisor(intB, aPraim);
}

function leastCommonMultiplyNaive(intA, intB) {
    var lcm;

    for (var l = 1; l <= (intA * intB); l++){
        if (l % intA === 0 && l % intB === 0) {
            lcm = l;
            break;
        }
    }
    return lcm;
}