/**
 * Created by HP on 10-Sep-17.
 */
while(true) {
    var intLimit = 2000000;
    var intA = Math.ceil(Math.random() * intLimit);
    var intB = Math.ceil(Math.random() * intLimit);

    var naiveImplementation = greatestCommonDivisorNaive(intA, intB);
    var fastImplementation  = greatestCommonDivisor(intA, intB);

    if (fastImplementation !== naiveImplementation) {
        console.log('\n\n');
        console.log(`WRONG ANSWER!!! A: ${intA}, B: ${intB}; FastImplementation: ${fastImplementation}, SlowImplementation: ${naiveImplementation}`);
        console.log('\n\n');
        break;
    }
    console.log('\n');
    console.log(`Both are same: ${naiveImplementation} = ${fastImplementation}`);
    console.log('\n');
}

function greatestCommonDivisor(intA, intB) {
    if (intB === 0) {
        return intA;
    }
    var aPraim = intA % intB;
    return greatestCommonDivisor(intB, aPraim);
}

function greatestCommonDivisorNaive(intA, intB) {
    var currentGCD = 1;

    for (var i = 2; i <= intA && i <= intB; i++) {
        if ((intA % i == 0) && (intB % i == 0)) {
            currentGCD = i;
        }
    }
    return currentGCD;
}