/**
 * Created by HP on 13-Oct-17.
 */
//'use strict'
let readline = require('readline');
let rl  = readline.createInterface(process.stdin, process.stdout);
let cl = 1;
let n = 0;
let vertexes = [];

rl.on('line', assigner);

function assigner (line) {
    if (cl === 1) {
        n = parseInt(line);
    }
    if (1 < cl && cl <= n + 1) {
        let element = line.trim().split(' ');
        element = element.map(Number);
        vertexes.push(element);
    }
    if (cl > n) {
        traverseTree(vertexes);
        rl.close();
        process.exit();
    }
    cl++;
}


class Node {
    constructor(node) {
        this.key  = node[0];
        this.lc   = node[1] === -1 ? false : node[1];
        this.rc   = node[2] === -1 ? false : node[2];
    }
}

function traverseTree(tree) {
    let tr = tree.map(function (node) {
        return new Node(node);
    });
    this.inOrderRes = [];
    this.postOrderRes = [];
    this.preOrderRes  = [];
    this.inOrder = function () {
       return inOTr(tr[0])
    };
    this.postOrder = function () {
       return postOrT(tr[0]);
    };
    this.preOrder = function () {
        return preOrder(tr[0]);
    };
    function inOTr(tree) {
        if (!tree) return;
        inOTr(tr[tree.lc]);
        this.inOrderRes.push(tree.key);
        inOTr(tr[tree.rc]);
        return this.inOrderRes;
    }
    function postOrT(tree) {
        if (!tree) return;
        postOrT(tr[tree.lc]);
        postOrT(tr[tree.rc]);
        this.postOrderRes.push(tree.key);
        return this.postOrderRes;
    }
    function preOrder(tree) {
        if (!tree) return;
        this.preOrderRes.push(tree.key);
        preOrder(tr[tree.lc]);
        preOrder(tr[tree.rc]);
        return this.preOrderRes;
    }
    console.log(this.inOrder().join(' '));
    console.log(this.preOrder().join(' '));
    console.log(this.postOrder().join(' '));
}