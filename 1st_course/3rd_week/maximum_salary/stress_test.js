
(function generator() {
    var everythingIsOk = true;

    while (everythingIsOk) {

        var randomN = Math.ceil(Math.random() * 6 + 1);
        var randomSequence = [];

        for (var s = 1; s < randomN; s++) {
            var randomElement = Math.ceil(Math.random() * 10);
            randomSequence.push(randomElement);
        }

        var clone = randomSequence.slice(0);
        var naiveSolution = makeMeWellPaidNaive(randomSequence, randomN);
        var fastSolution  = makeMeWellPaid(randomSequence, randomN);

        if(parseInt(naiveSolution) != parseInt(fastSolution)) {
            console.log('\n');
            console.log('\n');
            console.log('\n');
            console.log(`WRONG ANSWER: N: ${randomN};`);
            console.log(`Sequence ${clone.toString()};`);
            console.log(`Fast Solution  ${fastSolution};`);
            console.log(`Naive Solution ${naiveSolution};`);
            break;
        }

        console.log('\n');
        console.log(clone);
        console.log(`OK ${fastSolution} === ${naiveSolution}`);
    }

})();



function makeMeWellPaidNaive(sequence, n) {
    var currentMax = sequence.toString().replace(/,/g, '');

    for (var i = 0; i < factorial(n + 1); i++) {
        //var clone = sequence.slice(0);
        var indexes = [];

        while (indexes.length < sequence.length) {
            var randomIndex = Math.floor(Math.random() * (sequence.length));
            if(indexes.indexOf(randomIndex) === -1) {
                indexes.push(randomIndex);
            }
        }
        //console.log(indexes);

        var string = '';
        for (var j = 0; j < indexes.length; j++) {
            string += sequence[indexes[j]].toString();
            //string += ' ';
        }

        if (parseInt(string) > parseInt(currentMax)) {
            currentMax = string;
        }

    }
    return currentMax;
}


function factorial(n) {
    //console.log(n);
    if (n === 0) {
        return 1;
    }
    return factorial(n - 1) * n;
}


function makeMeWellPaid(sequence, n) {
    var answerString = '';

    while (sequence.length > 0) {
        var currentMax = sequence[0];

        for (var i = 1; i < sequence.length; i++) {
            if(greaterOrEqual(sequence[i], currentMax)) {
                currentMax = sequence[i];
            }
        }
        //answerString += ' ';
        answerString += currentMax.toString();
        sequence.splice(sequence.indexOf(currentMax), 1);
    }
    return answerString;
}

function greaterOrEqual(iteration, currentMax) {
    var singleDigitNumbers = iteration.toString().split('');
    var currentMaximum = currentMax.toString().split('');

    //console.log(singleDigitNumbers, currentMaximum);

    for (var c = 0; c < singleDigitNumbers.length && c < currentMaximum.length; c++) {
        var iterationDigit = parseInt(singleDigitNumbers[c]);
        var maximumDigit   = parseInt(currentMaximum[c]);

        if (iterationDigit !== maximumDigit) {
            return parseInt(singleDigitNumbers[c]) >=  parseInt(currentMaximum[c]);
        }

        if((iterationDigit === maximumDigit) && (singleDigitNumbers.length === currentMaximum.length)) {
            continue;
        }

        if((iterationDigit === maximumDigit) && (singleDigitNumbers.length !== currentMaximum.length)) {
            return checkUnequalNumbers(singleDigitNumbers, currentMaximum);
        }

    }

}

function checkUnequalNumbers(iteration, currentMax) {

    var iterationClone = iteration.slice(0);
    var currentMaxClone = currentMax.slice(0);

    while (iteration.length > 0 && currentMax.length > 0) {
        var iterationHigh = parseInt(iteration.shift());
        var maximumHigh   = parseInt(currentMax.shift());

        /*console.log(`IterationHigh: ${iterationHigh}, MaximumHigh: ${maximumHigh}`);
         console.log(iteration);
         console.log(currentMax);*/

        if (iterationHigh != maximumHigh) {
            return iterationHigh > maximumHigh;
        }
    }

    return stripAndCheck();

    function stripAndCheck() {
        if (iteration.length > currentMax.length) {
            //console.log('teration longer')
            for (var j = 0; j < iteration.length; j++) {
                if (iteration[j] === currentMaxClone[0]) {
                    continue;
                }
                return iteration[j] > currentMaxClone[0];
            }
            return iterationClone[0] > iterationClone[1]; // don't change
        }
        if (currentMax.length > iteration.length) {
            //console.log('maximum longer');
            for (var l = 0; l < currentMax.length; l++) {
                if(currentMax[l] === iterationClone[0]) {
                    continue;
                }
                return currentMax[l] < iterationClone[0];
            }
            return currentMaxClone[0] < currentMaxClone[1];  // don't change*/
        }
    }

}