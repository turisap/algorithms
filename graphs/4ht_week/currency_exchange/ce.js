/**
 * Created by HP on 17-Oct-17.
 */
let readline = require('readline');
let rl = readline.createInterface(process.stdin, process.stdout);
let cl = 1;
let n = 0;
let m = 0;
let u = 0;
let v = 0;
let list = [];

rl.on('line', assigner);

function assigner(line) {
    if (cl === 1) {
        let data = line.trim().split(' ');
        n = parseInt(data[0]);
        m = parseInt(data[1]);
    }
    if (1 < cl && cl <= m + 1) {
        let data = line.trim().split(' ').map(function(item, index) {
            if (index === 2) return parseInt(item); // weight of an edge should not be decreased
            return parseInt(item) - 1;
        });
        let t = data[2];
        data[2] = data[1];
        data[1] = t;
        list.push(data);
    }
    if (cl === m + 1) {
        findDistance();
        rl.close();
        process.exit();
    }
    cl++;
}

function PriorityQueue(sequence) {
    this.numbersOfSwaps = 0;
    this.swappedPairs = [];
    this.size = function() {
        return sequence.length - 1;
    };

    this.setDist = function(number, dist) {
        for (let i = 0; i < sequence.length; i++) {
            if (sequence[i].number === number ){
                sequence[i].distance = dist;
                break;
            }
        }
        this.buildMinHeap();
    };
    this.showSequence = function () {
        console.log(sequence);
    };
    this.isEmpty = function () {
        return sequence.length === 0;
    };
    this.buildMinHeap = function () {
        for (let i = Math.floor(this.size() / 2); i >= 0; i--) {
            this.siftDown(i);
        }
        return sequence;
    };
    this.extractMin = function () {
        let result = sequence[0];
        sequence[0] = sequence[sequence.length - 1];
        sequence.pop();
        this.siftDown(0);
        return result;
    };
    this.siftDown = function (index) {
        minIndex = index;
        let l = this.leftChild(minIndex);
        let r = this.rightChild(minIndex);

        if (l <= this.size() && sequence[l].distance < sequence[minIndex].distance) {
            minIndex = l;
        }
        //console.log()
        if (r <= this.size() && sequence[r].distance < sequence[minIndex].distance) {
            minIndex = r;
        }
        if (index != minIndex) {
            this.swap(index, minIndex);
            this.siftDown(minIndex);
        }
    };
    this.swap = function (indexA, indexB) {
        let temporaryElement = sequence[indexA];
        sequence[indexA] = sequence[indexB];
        sequence[indexB] = temporaryElement;
        this.numbersOfSwaps++;
        this.swappedPairs.push([indexA, indexB]);
    };
    this.parent = function (i) {
        return Math.floor((i - 1) / 2);
    };
    this.leftChild = function (i) {
        return (2 * i) + 1;
    };
    this.rightChild = function (i) {
        return (2 * i) + 2;
    };
}

function Graph(vNum, edges, vertexToFindPathFrom, vertexToFindPathTo) {
    let adjList =  makeAdjacentList();
    let nodes   = createNodes();
    let visitCounter = 1;
    this.isBipartite = true;
    this.hasNegativeCycle = false;
    this._BFS = function () {
        let q = new Queue(nodes.length - 1);
        let currentDistanceLyer = 1;
        let currentColor = 'white';
        q.enqueue(nodes[vertexToFindPathFrom]);
        nodes[vertexToFindPathFrom].distance = currentDistanceLyer;
        while(!q.isEmpty()){
            let current = q.dequeue();
            currentDistanceLyer = current.distance;
            adjList[current.number - 1].forEach( edge => {
                if (!nodes[edge].distance){
                    q.enqueue(nodes[edge]);
                    nodes[edge].distance = currentDistanceLyer + 1;
                }
            });
        }
        return nodes[vertexToFindPathTo].distance - 1;
    };
    this._BellmanFord = function (v = false) {
        if (this.hasNegativeCycle) return;
        let set = [...nodes];
        let start = v ? v : vertexToFindPathFrom;
        set[start].distance = 0;
        for (let i = 0; i < vNum - 1; i++) {
            let relaxed = 0;
            let checker = 0;
            adjList.forEach((el, index) => {
                for (let c = 0; c < el.length; c++){
                    let edge = el[c];
                    let current = set[index];
                    let relaxedDist = current.distance + edge.weigth;
                    if (set[edge.destination].distance > relaxedDist) {
                        set[edge.destination].distance = relaxedDist;
                        set[edge.destination].prevNode     = index;
                        relaxed ++;
                    }
                }
            });
            if (checker === relaxed) break;
        }
        adjList.forEach((el, index) => {
            for (let c = 0; c < el.length; c++){
                let edge = el[c];
                let current = set[index];
                let relaxedDist = current.distance + edge.weigth;
                if (set[edge.destination].distance > relaxedDist) {
                    this.hasNegativeCycle = true;
                }
            }
        });
        set.forEach((node, index) => {
            if (node.distance === Infinity) {
                this._BellmanFord(index);
            }
        });
        //console.log(set);
        //console.log(this.hasNegativeCycle);
    };
    this.topologicalSort = function () {
        this.componentsSearch();
        //console.log(nodes)
        let order = [...nodes];
        order.sort(function (a, b) {
            //return a.postVisit === b.postVisit ? 0 : (a.postVisit > b.postVisit ? 1 : -1)
            if(parseInt(a.postVisit) > parseInt(b.postVisit))   return 1;
            if(parseInt(a.postVisit) < parseInt(b.postVisit))   return -1;
        });
        order.reverse();
        return order;
    };
    this.isDAG = function () {
        this.componentsSearch();
        let isDAG = true;
        edges.forEach(edge => {
            //console.log(edge);
            if(nodes[edge[0]].postVisit < nodes[edge[1]].postVisit) isDAG = false;
        });
        return isDAG;
    };
    this.componentsSearch = function () {  // it looks like works only for undirected graphs
        let cN = 1;
        for (let i = 0; i < vNum; i++) {
            if (nodes[i].visited) continue;
            explore(i, cN);
            cN++;
        }
        //console.log(nodes);
        return (cN - 1);
    };
    this.shortestPath = function (u, v) {
        let clone = [...nodes];
        let q = new PriorityQueue(clone);
        q.setDist(nodes[u].number, 0);
        q.buildMinHeap();
        while (!q.isEmpty()) {
            let current = q.extractMin();
            for (let i = 0; i < adjList[current.number - 1].length; i++) {
                let edge = adjList[current.number - 1][i];
                let relaxedDist = current.distance + edge.weigth;
                if (nodes[edge.destination].distance > relaxedDist) {
                    nodes[edge.destination].distance = relaxedDist;
                    nodes[edge.destination].prevNode = current.number;
                    q.setDist(nodes[edge.destination].number, relaxedDist)
                }
            }
        }
        //console.log('========')
        //console.log(nodes);
        return nodes[v].distance === Infinity ? -1 : nodes[v].distance
    };
    function explore(v, componentN) {
        nodes[v].visited = true;
        nodes[v].componentNumber = componentN;
        preVisit(v);
        adjList[v].forEach((item) => {
            if (!nodes[item].visited){
                explore(item, componentN)
            }
        });
        postVisit(v);
        return nodes;
    }
    function Node(n) {
        this.number = n;
        //this.visited = false;
        this.distance = +Infinity;
        this.prevNode = false;
        //this.componentNumber = 0;
        //this.postVisit = 0;
    }
    function Edge (d, w) {
        this.destination = d;
        this.weigth = w;
    }
    function createNodes () {
        let res = [];
        for (let i = 0; i < vNum; i++){res.push(new Node(i + 1));}
        return res;
    }
    function makeAdjacentList() {
        let adjList = [];
        for (let i = 0; i < vNum; i++) {
            adjList.push([])
        }
        for (let c = 0; c < edges.length; c++) {
            if (edges[0].length === 3) {
                adjList[edges[c][0]].push(new Edge(edges[c][2], edges[c][1])); // change 2 to 1 if there are no weights for edges
                //adjList[edges[c][1]].push(edges[c][0]); // this line was for undirected graphs
            } else if(edges[0].length === 2) {
                adjList[edges[c][0]].push(edges[c][1]); // change 2 to 1 if there are no weights for edges
                //adjList[edges[c][1]].push(edges[c][0]); // this line was for undirected graphs
            }
        }
        //console.log(adjList);
        return adjList;
    }
    function preVisit(v){
        nodes[v].preVisit = visitCounter;
        visitCounter++;
    }
    function postVisit(v) {
        nodes[v].postVisit = visitCounter;
        visitCounter++;
    }
}

function findDistance() {
    let g = new Graph(n, list, u, v);
    g._BellmanFord();
    console.log(g.hasNegativeCycle ? 1 : 0);
}

