/**
 * Created by HP on 07-Sep-17.
 */


while(true) {
    var randomNumber = Math.ceil(Math.random() * 1000 + 2);
    var sequence = [];

    console.log(randomNumber);

    for (var i = 0; i < randomNumber; i++) {
        sequence.push(Math.ceil(Math.random() * 100000))
    }
    console.log('\n');
    console.log(sequence);

    var res1 = maxPairwaiseProduct(sequence);
    var res2 = maxPairwaiseProductNaive(sequence);

    if (res1 != res2) {
        console.log('Wrong Answer');
        console.log(`ResultFast ${res1}, ResultNaive ${res2}`);
        console.log('\n');
        break;
    } else {
        console.log('OK');
    }
}

function maxPairwaiseProduct(numbers) {

    if(numbers.length > 0) {
        var maxIndex1 = -1;
        var maxIndex2 = -1;

        for(var j = 0; j < numbers.length; j++) {
            if (maxIndex1 == -1  || numbers[j] > numbers[maxIndex1]) {
                maxIndex1 = j;
            }
        }

        for(var c = 0; c < numbers.length; c++){
            if ((maxIndex2 == -1 && c !== maxIndex1) || (c != maxIndex1 && (numbers[c] > numbers[maxIndex2]))) {
                maxIndex2 = c;
            }
        }

        result = numbers[maxIndex1] * numbers[maxIndex2];
        return result;
    }
}

function maxPairwaiseProductNaive(numbers) {

    if(numbers.length > 0) {
        for (var j = 0; j < numbers.length; j++){
            for (var c = j + 1; c < numbers.length; c++) {
                var multi = numbers[j] * numbers[c];
                result = (multi > result) ? multi : result;
            }
        }
        return result;
    }
}