/**
 * Created by HP on 10-Sep-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);

function helper(line) {
    var intA = parseInt(line.split(' ').shift());
    var intB = parseInt(line.split(' ').pop());

    console.log(greatestCommonDivisor(intA, intB));
    rl.close();
    process.exit();
}

function greatestCommonDivisor(intA, intB) {
    if (intB === 0) {
        return intA;
    }
    var aPraim = intA % intB;
    return greatestCommonDivisor(intB, aPraim);
}