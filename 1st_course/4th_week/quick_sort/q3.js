/**
 * Created by HP on 27-Sep-17.
 */

var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);

var currentLine = 1;
var sequenceLength = 0;
var sequence = [];

function helper(line) {
    if (currentLine === 1) {

    }
    if (currentLine === 2) {
        sequence = line.split(' ');
        sequence = sequence.map(function (item) {
            return parseInt(item);
        });
        sequenceLength = sequence.length - 1;
        console.log(quickSort3Partition(sequence, 0, sequenceLength));
        rl.close();
        process.exit();
    }
    currentLine++;
}

function quickSort3Partition (array, l, r) {

    if (l < r) {
        var borders = partition3(array, l, r);
        var lowBorder = borders.shift();
        var highBorder = borders.pop();
        quickSort3Partition(array, l, lowBorder - 1);
        quickSort3Partition(array, highBorder + 1, r);
    }
    return array.join(' ');
}

function swap(array, leftIndex, rightIndex) {
    var temp = array[leftIndex];
    array[leftIndex] = array[rightIndex];
    array[rightIndex] = temp;
}

function partition3 (array, left, right) {
    //console.log(array);
    var randomElementIndex = Math.floor(Math.random() * (right - left) + left);
    swap(array, randomElementIndex, left);
    var pivot = left;
    var j = left;

    //console.log(randomElementIndex);

    for (var counter = left + 1; counter <= right; counter++) {
        if (array[counter] <= array[pivot]) {
            j++;
            swap(array, j, counter);
        }
    }
    swap(array, j, pivot);
    //console.log(array);
    //console.log(`j = ${j}`);


    var newPlace = j - 1;
    for (var i = j - 1; i >= 0; i--) {
        if (array[i] === array[j]) {
            swap(array, i, newPlace);
            newPlace--;
        }
    }
    newPlace++;
    //console.log(array);

    return [newPlace , j];

}