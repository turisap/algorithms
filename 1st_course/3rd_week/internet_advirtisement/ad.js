/**
 * Created by HP on 14-Sep-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);
var currentLine = 1;
var n = 0;
var profitPerClick = [];
var clicks = [];

function helper(line) {

    if (currentLine === 1) {
        n = parseInt(line);
    }

    if (currentLine === 2) {
        profitPerClick = getProfitsArray(line);
    }

    if (currentLine == 3) {
        clicks = getClicksArray(line);
        getTonsOfCash(n, profitPerClick, clicks);

        /*console.log(n);
        console.log(profitPerClick);
        console.log(clicks);*/

        rl.close();
        process.exit();
    }
    currentLine++;
}


function getTonsOfCash(n, profitPerClick, clicks) {
    var overallCash = 0;

    for (var i = 1; i <= n; i++) {
        var largestProfit = Math.max.apply(Math, profitPerClick);
        var highestClickable = Math.max.apply(Math, clicks);

        //console.log(`${largestProfit} and ${highestClickable}`);
        overallCash += largestProfit * highestClickable;

        profitPerClick.splice(profitPerClick.indexOf(largestProfit), 1);
        clicks.splice(clicks.indexOf(highestClickable), 1)
    }
    console.log(overallCash);
}



function getProfitsArray (line) {
    return line.split(' ').map(Number);
}

function getClicksArray(line) {
    return line.split(' ').map(Number);
}

