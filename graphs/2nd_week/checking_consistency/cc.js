/**
 * Created by HP on 15-Oct-17.
 */
let readline = require('readline');
let async = require('async');
let rl = readline.createInterface(process.stdin, process.stdout);
let cl = 1;
let n = 0;
let m = 0;
let list = [];

rl.on('line', assigner);

function assigner(line) {
    if (cl === 1) {
        let data = line.trim().split(' ');
        n = parseInt(data[0]);
        m = parseInt(data[1]);
    }
    if (1 < cl && cl <= m + 1) {
        list.push(line.trim().split(' ').map(function(item) {
            return parseInt(item) - 1;
        }));
    }
    if (cl === m + 1) {
        topologicalSort();
        rl.close();
        process.exit();
    }
    cl++;
}

function Graph(vNum, edges) {
    let adjList =  makeAdjacentList();
    let nodes   = createNodes();
    let visitCounter = 1;
    this.topologicalSort = function () {
        this.componentsSearch();
        //console.log(nodes)
        let order = [...nodes];
        order.sort(function (a, b) {
            //return a.postVisit === b.postVisit ? 0 : (a.postVisit > b.postVisit ? 1 : -1)
            if(parseInt(a.postVisit) > parseInt(b.postVisit))   return 1;
            if(parseInt(a.postVisit) < parseInt(b.postVisit))   return -1;
        });
        order.reverse();
        return order;
    };
    this.isDAG = function () {
        this.componentsSearch();
        let isDAG = true;
        edges.forEach(edge => {
            //console.log(edge);
            if(nodes[edge[0]].postVisit < nodes[edge[1]].postVisit) isDAG = false;
        });
        return isDAG;
    };
    this.componentsSearch = function () {  // it looks like works only for undirected graphs
        let cN = 1;
        for (let i = 0; i < vNum; i++) {
            if (nodes[i].visited) continue;
            explore(i, cN);
            cN++;
        }
        //console.log(nodes);
        return (cN - 1);
    };
    function explore(v, componentN) {
        nodes[v].visited = true;
        nodes[v].componentNumber = componentN;
        preVisit(v);
        adjList[v].forEach((item) => {
            if (!nodes[item].visited){
                explore(item, componentN)
            }
        });
        postVisit(v);
        return nodes;
    }
    function ex(item, com){
        trampoline(function () {
            return explore(item, com)
        })
    }
    function Node(n) {
        this.number = n;
        this.visited = false;
        this.componentNumber = 0;
        this.preVisit = 0;
        this.postVisit = 0;
    }
    function createNodes () {
        let res = [];
        for (let i = 0; i < vNum; i++){res.push(new Node(i + 1));}
        return res;
    }
    function makeAdjacentList() {
        let adjList = [];
        for (let i = 0; i < vNum; i++) {
            adjList.push([])
        }
        for (let c = 0; c < edges.length; c++) {
            adjList[edges[c][0]].push(edges[c][1]);
            //adjList[edges[c][1] - 1].push(edges[c][0]); // this line was fo undirected graphs
        }
        return adjList;
    }
    function preVisit(v){
        nodes[v].preVisit = visitCounter;
        visitCounter++;
    }
    function postVisit(v) {
        nodes[v].postVisit = visitCounter;
        visitCounter++;
    }
    function trampoline(fn) {
        while(fn && typeof fn === 'function') {
            fn = fn()
        }
    }
}

function topologicalSort(){
    let res = [];
    let gr = new Graph(n, list);
    let sortedNodes = gr.topologicalSort();
    sortedNodes.forEach(function (node) {
        res.push(node.number)
    });
    console.log(res.join(' '));
}