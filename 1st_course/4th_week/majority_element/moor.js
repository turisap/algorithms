/**
 * Created by HP on 25-Sep-17.
 */
var readline  = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', majorityElement);

var currentLine = 1;
var sequenceLength = 0;

function majorityElement(line) {
    this.sequence;
    this.init = function (line) {
        this.assigner(line);
    };

    this.assigner = function (line) {
        if (currentLine === 2) {
            this.sequence = line.split(' ');
            this.sequence = this.sequence.map(function (item) {
                return parseInt(item);
            });
            this.sequenceClone = this.sequence.slice(0);
            if (this.findMajorityElement(this.sequence)) {
                console.log(1);
            } else {
                console.log(0);
            }
            //console.log(this.findMajorityElement(this.sequence));
            rl.close();
            process.exit();
        }
        if (currentLine === 1) {
            sequenceLength = parseInt(line);
            currentLine++;
        }
    };

    this.findMajorityElement = function (sequence) {
        var counter = 0;
        var candidate = null;
        for (var i = 0; i < sequence.length; i++) {
            if (counter === 0) {
                candidate = sequence[i];
                counter = 1;
            } else {
                candidate === sequence[i] ? counter++ : counter-- ;
            }
            //console.log(`Candidate: ${candidate}`)
        }

        var proof = 0;
        for (var j = 0; j < sequence.length; j++) {
            if (sequence[j] === candidate) {
                proof++;
            }
        }
        //console.log(proof)
        var minNumber = Math.floor(sequence.length / 2);
        return proof > minNumber;
    };


    this.init(line);
}