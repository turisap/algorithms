/**
 * Created by HP on 25-Sep-17.
 */
var bigInt = require('big-integer');

(function generator() {

    this.init = function () {
        this.looper()
    }

    this.getRandoms = function () {
        var searchableLength  = Math.floor(Math.random() * 100);
        var valuesLength      = Math.floor(Math.random() * 100);

        var sequence = [0];
        var values   = [];

        var randomParam = Math.floor(Math.random() * 5 + 1);
        for (var i = 1; i < searchableLength; i++) {
            sequence.push(randomParam * i);
        }
        for (var c = 0; c < valuesLength; c++) {
            values.push((Math.floor(Math.random() * 100)));
        }
        sequence = sequence.sort(function(a, b){return a - b});
        return [sequence, values];
    }

    this.looper = function () {
        while (true) {
            var randoms = this.getRandoms();
            var sequence = randoms.shift();
            var values = randoms.pop();
            var fastSolution = findValues(sequence, values);
            var naiveSolution = searchNaive(sequence, values);

            if (fastSolution != naiveSolution) {
                console.log('\n');
                console.log('ERROR!!!!');
                /*console.log(sequence);
                console.log(values.map(function (item) {
                    return parseInt(item.toString());
                }));*/
                console.log(`FastSolution: ${fastSolution}`);
                console.log(`NaiveSolution: ${naiveSolution}`);
                break;
            }

            console.log('OK');
            console.log(`${naiveSolution} === ${fastSolution}`);
            /*console.log(sequence);
            console.log('===================');
            console.log(values);*/
        }
    };


    this.init();
})();

function findValues(array, values){
    var results = [];
    values.forEach(function(item) {
        results.push(binarySearch(array, 0, (array.length - 1), item));
    });
    return results.join(' ');
}


function binarySearch(array, low, high, target) {

    if (high < low) {
        return  -1;
    }

    var guess = Math.floor((high + low) / 2);

    if (targetLessThanGuess(target, array, guess)) {
        return binarySearch(array, low, (guess - 1), target);
    }
    if (targetMoreThanGuess(target, array, guess)) {
        return binarySearch(array, (guess + 1), high, target);
    }
    if (targetEqaulGuess(target, array, guess)) {
        return guess;
    }
}


function targetMoreThanGuess(target, array, guess) {
    //return target.compare(array[guess]) === 1;
    return parseInt(target) > array[guess];
}
function targetLessThanGuess(target, array, guess) {
    return parseInt(target) < array[guess];
    //return target.compare(array[guess]) === -1;
}
function targetEqaulGuess(target, array, guess) {
    /*console.log(`Guess: ${array[guess]}`);
     console.log(`target: ${target}`);*/
    return target === array[guess];
}
/** ============================ THE NAIVE SOLUTION ====================== **/

function searchNaive(searchArray, searchValues) {
    //console.log(searchArray, searchValues);
    var resultsArray = [];

    for (var i = 0; i < searchValues.length; i++) {
        resultsArray.push(findValueInArray(searchValues[i], searchArray));
    }
    return resultsArray.join(' ');
}

function findValueInArray(value, array) {
    for (var c = 0; c < array.length; c++) {
        if (value === (array[c])) {
            return c;
        }
    }
    return -1;
}