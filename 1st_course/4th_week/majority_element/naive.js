/**
 * Created by HP on 25-Sep-17.
 */
var readline  = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', majorityElementNaive);

var currentLine = 1;
var sequenceLength = 0;
function majorityElementNaive(line) {
    this.sequence = [];

    this.init = function (line) {
        this.assigner(line);
    };

    this.assigner = function (line) {
        if (currentLine === 2) {
            this.sequence = line.split(' ');
            this.findMajorityElement(sequenceLength, this.sequence);
            rl.close();
            process.exit();
        }
        if (currentLine === 1) {
            sequenceLength = parseInt(line);
            currentLine++;
        }
    };

    this.findMajorityElement = function (n, sequence) {
      /*console.log(n);
      console.log(sequence);*/

      var minimumMajority = this.findMinMajorityNumber(n);

      for (var i = 0; i < sequence.length; i++) {
          var currentElement = sequence[i];
          var count = 1;
          for (var j = i + 1; j < sequence.length; j++) {
              if (sequence[j] === currentElement) {
                  count++
              }
          }
          if (count >= minimumMajority) {
              console.log(1);
              process.exit();
          }
      }
      console.log(0);
    };

    this.findMinMajorityNumber = function (n) {
      return (n % 2 === 0) ? ((n / 2) + 1) : Math.ceil(n / 2);
    };

    this.init(line);
}