/**
 * Created by HP on 09-Oct-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', assigner);

var currentLine = 1;
var n = 0;
var operations = [];

function assigner(line) {
    if(currentLine === 1) {
        n = parseInt(line.trim());
    } else if (0 < currentLine <= n) {
        operations.push(line.trim());
    }
    if (currentLine === n + 1) {
        processPhoneBook(operations, n);
        rl.close();
        process.exit();
    }
    currentLine++;
}

function PhoneBook(size) {
    this.vessel = new Array (size - 1).fill('not found');
    this.find = function (number) {
        return this.vessel[number];
    };
    this.add  = function (number, name) {
        this.vessel[number] = name;
    };
    this.delete = function (number) {
        this.vessel[number] = 'not found';
    }
}

function processPhoneBook(operations, n) {
    var book = new PhoneBook(10000000);
    operations.forEach(function(operation) {
       var data = operation.split(' ');
       if(data[0] === 'find') {
           var requestedPerson = book.find(data.pop());
           console.log(requestedPerson);
       }
       if (data[0] === 'del') {
           book.delete(data.pop());
       }
       if (data.shift() === 'add') {
           book.add(data.shift(), data.shift());
       }
    });
}