/**
 * Created by HP on 10-Oct-17.
 */
var readline = require('readline');
var bigInt = require('big-integer');
var rl = readline.createInterface(process.stdin, process.stdout);
rl.on('line', assigner);

var currentLine = 1;
var pattern = '';
var text = '';
function assigner(line) {
    if (currentLine === 1) {
        pattern = line.trim();
    }
    if (currentLine === 2) {
        text = line.trim();
        //findPatternsInText(pattern, text);
        textChecker(pattern, text);
        rl.close();
        process.exit();
    }
    currentLine++;
}

function textChecker(pattern, text) {
    var result = [];
    var p = parseInt(bigInt(pattern.length * text.length).pow(2).toString());
    var patternHash = polyHash(pattern, p, 5);
    for (var i = 0, len = text.length - pattern.length; i <= len; i++) {
        var currentHash = polyHash(text.substring(i, i + pattern.length), p, 5);
        if (currentHash !== patternHash) {continue;}
        if(areEqual(text.substring(i, i + pattern.length), pattern)) {result.push(i)}
    }

    function polyHash(P,p,x) {
        var hash = 0;
        for (var i = 0; i < P.length; i++) {
            var current = bigInt(x).pow(i).multiply(parseInt(P.charCodeAt(i)));
            hash = current.add(hash);
        }
        hash = hash.mod(p);
        return parseInt(hash.toString());
    }
    function areEqual(substring, pattern) {
        if (substring.length != pattern.length) {
            return false;
        }
        for (var i = 0, len = pattern.length; i < len; i++) {
            if (substring.charAt(i) !== pattern.charAt(i)) {
                return false;
            }
        }
        return true;
    }
    console.log(result.join(' '));
}