/**
 * Created by HP on 13-Oct-17.
 */
//'use strict'
let readline = require('readline');
let rl  = readline.createInterface(process.stdin, process.stdout);
let cl = 1;
let n = 0;
let vertexes = [];

rl.on('line', assigner);

function assigner (line) {
    if (cl === 1) {
        n = parseInt(line);
    }
    if (1 < cl && cl <= n + 1) {
        let element = line.trim().split(' ');
        element = element.map(Number);
        vertexes.push(element);
    }
    if (cl > n) {
        traverseTree();
        rl.close();
        process.exit();
    }
    cl++;
}


let _tree = new WeakMap();
const getRightChild = Symbol('getRightChild');
const getLeftChild  = Symbol('getLeftChild');
const inOrderTraversalReal = Symbol('inOrderTraversalReal');
const preOrderTraversalReal = Symbol('preOrderTraversalReal');
const postOrderTraversalReal = Symbol('postOrderTraversalReal');

class Tree {
    constructor(tree) {
        this.inOrderStack = [];
        this.preOrderStack = [];
        this.postOrderStack = [];
        _tree.set(this, vertexes);
    }

    inOrderTraversal() {
        const tree = _tree.get(this).slice();
        let current = tree[0];
        let st = [];
        let isDone = false;
        while(!isDone) {
            if (current !== null) {
                st.push(current);
                current = this[getLeftChild](current);
            } else {
                if(st.length > 0) {
                    current = st.pop();
                    this.inOrderStack.push(current[0]);
                    current = this[getRightChild](current)
                } else {
                    isDone = true;
                }
            }
        }
        return this.inOrderStack;
    }

    preOrderTraversal() {
        const tree = _tree.get(this).slice();
        let current = tree[0];
        let st = [];
        let isDone = false;

        while (!isDone) {
            if (current !== null) {
                st.push(current);
                this.preOrderStack.push(current[0]);
                current = this[getLeftChild](current);
            } else {
                if(st.length > 0) {
                    current = st.pop();
                    current = this[getRightChild](current);
                } else {
                    isDone = true;
                }
            }
        }
        return this.preOrderStack;
    }

    postOrderTraversal() {
        const tree = _tree.get(this).slice();
        let st = [tree[0]];

        while (st.length > 0) {
            let lCh = this[getLeftChild](st.slice(-1)[0]);
            let rCh = this[getRightChild](st.slice(-1)[0]);
            this.postOrderStack.push(st.pop()[0]);
            if (lCh !== null ) st.push(lCh);
            if (rCh !== null ) st.push(rCh);
        }
        return this.postOrderStack.reverse();
    }
/* cd xampp/htdocs/courses/algorithms/data_structures/6th_week/tree_traversal */
    [getRightChild](tree){
        if (tree[2] === -1) return null;
        return _tree.get(this)[tree[2]];
    }
    [getLeftChild](tree){
        if (tree[1] === -1) return null;
        //console.log(tree[1]);
        return _tree.get(this)[tree[1]];
    }
}


function traverseTree() {
    let t = new Tree(vertexes);
    console.log(t.inOrderTraversal().join(' '));
    console.log(t.preOrderTraversal().join(' '));
    console.log(t.postOrderTraversal().join(' '));
}