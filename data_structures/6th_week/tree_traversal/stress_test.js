/**
 * Created by HP on 15-Oct-17.
 */
class Node {
    constructor(node) {
        this.key  = node[0];
        this.lc   = node[1] === -1 ? false : node[1];
        this.rc   = node[2] === -1 ? false : node[2];
    }
}
(function generator(){
    let tree = [];
    try {
        while (true) {
            let length = 100000;
            for (let i = 0; i < length; i++) {
                let element = Math.floor(Math.random() * 2);
                let node =[element];
                node.push(getLch(i) >= length ? -1 : getLch(i));
                node.push(getRch(i) >= length ? -1 : getRch(i));
                tree.push(node);
            }
            let r = traverseTree(tree);
            let inO = r[0];
            let pre = r[1];
            let post = r[2];
            if (check(inO) || check(pre) || check(post)) {
                console.log("ERROR")
                console.log(inO.join(' '));
                console.log(pre.join(' '));
                console.log(post.join(' '));
                break;
            }
            //console.log(inO.length, pre.length, post.length)
            console.log(inO.join(' '));
            console.log(pre.join(' '));
            console.log(post.join(' '));
        }
    } catch (err) {
        console.log(err.message);
        console.log(tree.length);
    }

    function getLch(i) {return 2 * i + 1}
    function getRch(i) {return 2 * i + 2}
    function check(array){
        for (let c = 0; c < array.length; c++){
            if (isNaN(array[c]) || array[c] === undefined){
                //console.log(array[c])
                return true}
        }
        return false;
    }
})();


function traverseTree(tree) {
    let tr = tree.map(function (node) {
        return new Node(node);
    });
    this.inOrderRes = [];
    this.postOrderRes = [];
    this.preOrderRes  = [];
    this.inOrder = function () {
        return inOTr(tr[0])
    };
    this.postOrder = function () {
        return postOrT(tr[0]);
    };
    this.preOrder = function () {
        return preOrder(tr[0]);
    };
    function inOTr(tree) {
        if (!tree) return;
        inOTr(tr[tree.lc]);
        this.inOrderRes.push(tree.key);
        inOTr(tr[tree.rc]);
        return this.inOrderRes;
    }
    function postOrT(tree) {
        if (!tree) return;
        postOrT(tr[tree.lc]);
        postOrT(tr[tree.rc]);
        this.postOrderRes.push(tree.key);
        return this.postOrderRes;
    }
    function preOrder(tree) {
        if (!tree) return;
        this.preOrderRes.push(tree.key);
        preOrder(tr[tree.lc]);
        preOrder(tr[tree.rc]);
        return this.preOrderRes;
    }
    return [this.inOrder(), this.preOrder(), this.postOrder()]
}