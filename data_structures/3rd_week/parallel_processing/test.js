/**
 * Created by HP on 07-Oct-17.
 */
(function generator(){

    while (true) {
        var tree = [0,1,2];
        var operation = Math.floor(Math.random() * 4);
        var length = Math.floor(Math.random() * 20 + 1);

        for (var i = 3; i <= length; i ++){
            var parentIndex = Math.floor((i - 1) / 2);
            tree.push(tree[parentIndex] + Math.floor(Math.random() * 10));
        }
        var qu = new PriorityQueue(tree);
        if (operation < 1) {
            qu.insert(Math.floor(Math.random() * 20));
        } else {
            qu.extractMin();
        }
        if (qu.isMinHeap()) {
            console.log(`OK, sequence is a min-heap. Operation is ${operation}`);
        } else {
            console.log('ERROR!!!!! The given sequence is not a heap!!!!!');
            break;
        }
    }

})();

function PriorityQueue (sequence)  {
    this.size = sequence.length - 1;
    this.insert = function (element) {
        if (sequence.length === this.size) {console.log('error inserting in the queue. capacity overflow');}
        var newLength = sequence.push(element);
        siftUp(newLength - 1);
        //console.log(sequence);
    };
    this.extractMin = function () {
        var result = sequence[0];
        sequence[0] = sequence[sequence.length - 1];
        sequence.pop();
        siftDown(0);
        //console.log(sequence);
        return result;
    };
    this.isMinHeap = function () {
        for (var c = 1, len = sequence.length; c < len; c ++) {
            if (sequence[parent(c)] > sequence[c]) {return false;}
        }
        return true;
    };
    function siftUp (i){
        //console.log(`Parent = ${sequence[parent(i)]}, element = ${sequence[i]}`);
        while (i > 0 && sequence[parent(i)] > sequence[i]) {
            swap(parent(i), i);
            i = parent(i);
        }
        //console.log(sequence)
    }
    function siftDown (index) {
        var minIndex = index;
        var l = leftChild(minIndex);
        var r = rightChild(minIndex);

        if (l <= sequence.length - 1 && sequence[l] < sequence[minIndex]) {
            minIndex = l;
        }
        if (r <= sequence.length - 1 && sequence[r] < sequence[minIndex]) {
            minIndex = r;
        }
        if (index != minIndex) {
            swap(index, minIndex);
            siftDown(minIndex);
        }
    }
    function swap(indexA, indexB) {
        var temporaryElement = sequence[indexA];
        sequence[indexA] = sequence[indexB];
        sequence[indexB] = temporaryElement;
    }
    function parent(i) {
        return Math.floor((i - 1) / 2);
    }
    function leftChild(i) {
        return (2 * i) + 1;
    }
    function rightChild(i) {
        return (2 * i) + 2;
    }
}

