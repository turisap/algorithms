/**
 * Created by HP on 10-Sep-17.
 */

while(true) {
    var random = Math.ceil(Math.random() * 70);

    var naive = lastFibonacciNumberNaive(random);
    var fast  = lastFibonacciNumber(random);

    if (naive != fast) {
        console.log(`\n\n\n`);
        console.log(` WRONG ANSWER!!! N: ${random} Naive solution: ${naive}; Fast solution: ${fast}`);
        break;
    }
    console.log('\n');
    console.log('\n');
    console.log(`N: ${random} Both are same ${naive} = ${fast}`);
}


function lastFibonacciNumberNaive (line) {
    var num = parseInt(line);
    if (checkIfFirstOrSecond(num)) {
        return num;
    }

    var previous = 0;
    var current  = 1;

    for (var i = 0; i < (num - 1); i++) {
        var temporaryPrevious = previous;
        previous  = current;
        current   = current + temporaryPrevious;
    }


    function checkIfFirstOrSecond(number) {
        return (number == 0 || number == 1);
    }

    return current.toString().split('').pop();
}

function lastFibonacciNumber (line) {
    var num = parseInt(line);
    if (checkIfFirstOrSecond(num)) {
        return num;
    }

    var m = 10000000000;
    var previous = 0;
    var current  = 1;
    var digits = [0,1];

    for (var i = 0; i < (num); i++) {
        var temporaryPrevious = previous % m;
        previous  = current % m;
        current   = (current + temporaryPrevious);
        digits.push(current.toString().split('').pop());
    }

    function checkIfFirstOrSecond(number) {
        return (number == 0 || number == 1);
    }

    return digits[num];
}