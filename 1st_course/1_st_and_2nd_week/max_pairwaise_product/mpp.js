/**
 * Created by HP on 07-Sep-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', maxPairwaiseProduct);

var i = 0;
var sequenceLenght = false;
var numbers = [];
var result = 0;

function maxPairwaiseProduct(line) {

    if(line !== '\n' && i === 1) {
        var n = line.trim().split(' ');
        n.forEach(function (item) {
            numbers.push(parseInt(item));
        })
    }
    if (line !== '\n' && i === 0) {
        sequenceLenght = parseInt(line.trim());
        i++;
    }

    if(sequenceLenght && numbers.length > 0) {
        var maxIndex1 = -1;
        var maxIndex2 = -1;

        for(var j = 0; j < numbers.length; j++) {
            if (maxIndex1 == -1  || numbers[j] > numbers[maxIndex1]) {
                maxIndex1 = j;
            }
        }

        for(var c = 0; c < numbers.length; c++){
            if ((maxIndex2 == -1 && c !== maxIndex1) || (c != maxIndex1 && (numbers[c] > numbers[maxIndex2]))) {
                maxIndex2 = c;
            }
        }

        result = numbers[maxIndex1] * numbers[maxIndex2];

        console.log(result);
        process.exit();
    }
}