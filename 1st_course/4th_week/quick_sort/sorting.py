#python3
import sys
import random

def hoarePartition(a, l, r):
    
    i = l
    j = r
   
    while True:
        if i >= r: break
        #print("i = ({}); a[i] = ({}); a[l] = ({}); j = ({})".format(i, a[i], a[l], j))
        while a[i] <= a[l]:
            i += 1
            if i == r: break
        while a[j] > a[l]:
            j -= 1
            if j == l: break
        if i >= j:
            break
        
        a[i], a[j] = a[j], a[i]
    a[l], a[j] = a[j], a[l]
    return j
    
    

def randomized_quick_sort(a, l, r):

    if l >= r:
        return
    
    if(arrayIsSorted(a, l, r)):
        #print 'dk'
        return a
   
    m = hoarePartition(a, l, r)
    #print list
    randomized_quick_sort(a, l, m - 1);
    randomized_quick_sort(a, m + 1, r);

def arrayIsSorted(a, l, r):
    for i in range(l, r):
        if i == r: break
        if a[i] > a[i + 1]:
            return False
    return True

if __name__ == '__main__':
    input = sys.stdin.read()
    n, *a = list(map(int, input.split()))
    #a = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
    randomized_quick_sort(a, 0, n - 1)
    for x in a:
        print(x, end=' ')
