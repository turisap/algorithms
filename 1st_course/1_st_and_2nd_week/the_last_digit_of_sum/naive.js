/**
 * Created by HP on 13-Sep-17.
 */
var readline = require('readline');
var bigInt   = require('big-integer');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);

function helper(line) {
    var n = bigInt(line);
    console.log(lastDigitOfFibonacciSumNaive(n));

    rl.close();
    process.exit();
}

function lastDigitOfFibonacciSumNaive(n) {
    if (n.toString() <= 1) {
        return n.toString();
    }

    var previous = bigInt(0);
    var current  = bigInt(1);
    var sum      = bigInt(1);
    var loopLimit = parseInt(n.toString() - 1);
    //console.log(`\n\n\n ${parseInt(n.toString() - 1)} \n\n\n`);

    for (var i = 0; i < loopLimit; i++) {
        var tempPrevious = previous;
        previous = current;
        current  = current.add(tempPrevious);
        sum = sum.add(current);
    }
    return (sum.mod(10)).toString();
}