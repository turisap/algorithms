/**
 * Created by HP on 03-Oct-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', checkBrackets);

function checkBrackets(line) {
    this.init = function () {
        console.log(this.checker(line));
        rl.close();
        process.exit();
    };


    this.checker = function (line) {
        var string = line.trim().split('');
        var stack = [];

        for (var i = 0; i < string.length; i++) {
            var currentCharacter = string[i];
            if (this.characterIsAnOpeningBracket(currentCharacter)){
                stack.push(currentCharacter);
            } else if (this.characterIsAClosingBracket(currentCharacter)) {
                if (stack.length === 0){return i + 1;}
                if (! matchedBrackets(stack.pop(), currentCharacter)){return i + 1;}
            }
        }
        if (stack.length > 0) {
            return findFirstUnmatchedOpenBracket(stack[0]);
        }
        return 'Success';

        function findFirstUnmatchedOpenBracket(bracket) {
            var counter = 0;
            indexOfUnmatched = 0;

            for (var c = 0; c < string.length; c++) {
                var currentChar = string[c];
                if (counter === 0 && currentChar === bracket) {
                    indexOfUnmatched = c;
                    counter++
                } else if(matchedBrackets(bracket, currentChar)) {
                    //console.log('call from here');
                    counter--;
                } else if(currentChar === bracket) {counter++}
            }
            return indexOfUnmatched + 1;
        }

        function matchedBrackets(firstCharacter, secondCharacter) {
            if(firstCharacter === '('){return secondCharacter === ')';}
            if(firstCharacter === '['){return secondCharacter === ']';}
            if(firstCharacter === '{'){return secondCharacter === '}';}
        }
    };

    this.characterIsAnOpeningBracket = function (character) {
        var test = new RegExp(/[\(\{\[]/);
        return test.test(character);
    };

    this.characterIsAClosingBracket = function (character) {
        var test = new RegExp(/[\]\)\}]/);
        return test.test(character);
    };


    this.init();
}