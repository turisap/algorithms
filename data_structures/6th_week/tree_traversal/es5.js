/**
 * Created by HP on 14-Oct-17.
 */

var readline = require('readline');
var rl  = readline.createInterface(process.stdin, process.stdout);
var cl = 1;
var n = 0;
var vertexes = [];

rl.on('line', assigner);

function assigner (line) {
    if (cl === 1) {
        n = parseInt(line);
    }
    if (1 < cl && cl <= n + 1) {
        var element = line.trim().split(' ');
        element = element.map(Number);
        vertexes.push(element);
    }
    if (cl > n) {
        traverseTree();
        rl.close();
        process.exit();
    }
    cl++;
}

function Tree (tree) {
    var treeM = tree;
    this.inOrderStack = [];
    this.postOrderStack = [];
    this.preOrderStack = [];
    this.inOrderTraversal = function (tree) {
        if (!tree) return;
        this.inOrderTraversal(getLeftChild(tree));
        this.inOrderStack.push(tree[0]);
        this.inOrderTraversal(getRightChild(tree));
        return this.inOrderStack;
    };
    this.postOrderTraversal = function (tree) {
        if (!tree) return;
        this.postOrderTraversal(getLeftChild(tree));
        this.postOrderTraversal(getRightChild(tree));
        this.postOrderStack.push(tree[0]);
        return this.postOrderStack;
    };
    this.preOrderTraversal = function (tree) {
        if (!tree) return;
        this.preOrderStack.push(tree[0]);
        this.preOrderTraversal(getLeftChild(tree));
        this.preOrderTraversal(getRightChild(tree));
        return this.preOrderStack;
    };
    function getLeftChild(tree) {
        if (tree[1] === -1) return false;
        return treeM[tree[1]];
    }
    function getRightChild(tree) {
        if (tree[2] === -1) return false;
        return treeM[tree[2]];
    }
}

function traverseTree() {
    var t = new Tree(vertexes);
    console.log(t.inOrderTraversal(vertexes[0]).join(' '));
    console.log(t.preOrderTraversal(vertexes[0]).join(' '));
    console.log(t.postOrderTraversal(vertexes[0]).join(' '));
}