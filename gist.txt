forEach(task, index) => console.log(task, index) // index shows iteration
helpers filter, find, map, forEach
function(...args) // array of arguments
const command = ['add', 3095309, 'me']; const[operation, number, command] = command; console.log(command) // array destruction
const line = [5, -1, 3]; const[key, leftChild, rightChild]; add(key) // array destruction
[...brands, ...marks] // array merging and pushing
let discount = .10 //floats declaration
function (price = 10, discount = getDiscount()) // function as a default arg of another function
let data = {price: 10, discount: 5}; let {price, discount} = data // object destructuring to vars
function({price, data}) // object destruction to a function arguments
string.includes(), string.startsWith(), string.endsWith()
generators *functions for iterations or array creations


while (processNextItem() > 0); loop without block(function inside returns a number);
let x = 23 || getX();

if you assigned a fun to a propery like this.nodes = function() {return getNodes()} you should use it as tree.nodes() not tree.nodes
for (let i = 0; i < 1e7; i++)
for (let index of allImgs)
arr = [1,3,4] arr2 = [...arr] cloning arrays instead of slice();
~~4.9 === 4  //true Math.floor