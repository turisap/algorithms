/**
 * Created by HP on 25-Sep-17.
 */
var readline = require('readline');
var bigInt   = require('big-integer');

var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);

var currentLine = 1;
var searchArray = null;
var searchValues = null;

function helper(line) {

    if (currentLine === 1) {
        searchArray = line.trim().split(' ');
        searchArray = searchArray.map(function (item) {
            return bigInt(item);
        });
        searchArray.shift();
        //searchArray.sort();
        //console.log(searchArray);
    }

    if (currentLine === 2) {
        searchValues = line.trim().split(' ');
        searchValues.shift();
        searchValues = searchValues.map(function (item) {
            return bigInt(item);
        });
        console.log(findValues(searchArray, searchValues));
        rl.close();
        process.exit();
    }
    currentLine++;
}

function findValues(array, values){
    var results = [];
    values.forEach(function(item) {
        results.push(binarySearch(array, item));
    });
    return results.join(' ');
}


function binarySearch(array, target) {
    ///console.log(array)
    var min = 0;
    var max = array.length - 1;
    var guess;

    while(max >= min) {
        guess = Math.floor((min + max) / 2);
        if (array[guess].compare(target) === 0) {
            return guess;
        } else if (array[guess].compare(target) === -1) {
            min = guess + 1;
        } else {
            max = guess - 1;
        }
    }
    return -1;
}


