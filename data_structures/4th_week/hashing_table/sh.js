/**
 * Created by HP on 09-Oct-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);
var bigInt = require('big-integer');

rl.on('line', assigner);

var currentLine = 1;
var hashTableSize = 0;
var nq = 0;
var queries = [];
function assigner(line) {
    if (currentLine === 1) {
        hashTableSize = parseInt(line.trim());
    } else if (currentLine === 2) {
        nq = parseInt(line.trim());
    } else if (2 < currentLine <= nq + 1) {
        queries.push(line.trim());
    }
    if (currentLine === nq + 2) {
        queryHashTable(queries, hashTableSize);
        rl.close();
        process.exit();
    }
    currentLine++;
}

function HashFunction (x, p, m) {
    this.getHash = function (string) {
        var hash = 0;
        for (var i = 0; i < string.length; i++) {
            var current = bigInt(x).pow(i).multiply(parseInt(string.charCodeAt(i)));
            hash = current.add(hash);
        }
        hash = hash.mod(p).mod(m);
        return parseInt(hash.toString());
    }
}

function HashTable(hashFunction, table) {
    this.table = table;
    this.addString = function (string) {
        var list = this.table[hashFunction.getHash(string)];
        for (var i = 0, len = list.length; i < len; i++) {
            if(list[i] === string) {return true;}
        }
        list.unshift(string);
    };
    this.check = function (i) {
        return this.table[i];
    };
    this.find = function (string) {
        var list = this.table[hashFunction.getHash(string)];
        for (var i = 0, len = list.length; i < len; i++) {
            if(list[i] === string) {return true;}
        }
        return false;
    };
    this.delete = function (string) {
        var list = this.table[hashFunction.getHash(string)];
        for (var i = 0, len = list.length; i < len; i++) {
            if (list[i] === string) {list.splice(i, 1); break;}
        }
    }
}

function queryHashTable(queries, hashTableSize) {
    var hashFunction = new HashFunction(263, 1000000007, hashTableSize);
    var buckets = [];
    for (var i = 0, len = hashTableSize; i < len; i++) {
        buckets.push([]);
    }
    var hashTable = new HashTable(hashFunction, buckets);

    for (var c = 0, lq = queries.length; c < lq; c++) {
        var currentQuery = queries[c].split(' ');
        if (currentQuery[0] === 'add') {hashTable.addString(currentQuery.pop());}
        else if (currentQuery[0] === 'check') {console.log(hashTable.check(currentQuery.pop()).join(' '));}
        else if (currentQuery[0] === 'find') {console.log(hashTable.find(currentQuery.pop()) ? 'yes' : 'no')}
        else if(currentQuery[0] === 'del') {hashTable.delete(currentQuery.pop())}
    }
}