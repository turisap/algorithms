/**
 * Created by HP on 09-Sep-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);


function helper(line){
    console.log(lastFibonacciNumber(line));
    rl.close();
    process.exit();
}

function lastFibonacciNumber (line) {

    var num = parseInt(line);
    if (checkIfFirstOrSecond(num)) {
        return num;
    }

    var m = 10000000000;
    var previous = 0;
    var current  = 1;
    var digits = [0,1];

    for (var i = 0; i < (num); i++) {
        var temporaryPrevious = previous % m;
        previous  = current % m;
        current   = (current + temporaryPrevious);
        digits.push(current.toString().split('').pop());
    }

    function checkIfFirstOrSecond(number) {
        return (number == 0 || number == 1);
    }

    return digits[num];
}





