/**
 * Created by HP on 15-Oct-17.
 */
let readline = require('readline');
let rl = readline.createInterface(process.stdin, process.stdout);
let cl = 1;
let n = 0;
let m = 0;
let list = [];

rl.on('line', assigner);

function assigner(line) {
    if (cl === 1) {
        let data = line.trim().split(' ');
        n = parseInt(data[0]);
        m = parseInt(data[1]);
    }
    if (1 < cl && cl <= m + 1) {
        list.push(line.trim().split(' ').map(Number));
    }
    if (cl === m + 1) {
        pathFinder();
        rl.close();
        process.exit();
    }
    cl++;
}

function Graph(vNum, edges) {
    let adjList =  makeAdjacentList();
    let nodes   = createNodes();
    this.componentsSearch = function () {
        let cN = 1;
        for (let i = 1; i <= vNum; i++) {
            if (nodes[i - 1].visited) continue;
            explore(i, cN);
            cN++;
        }
        //console.log(nodes);
        return (cN - 1);
    };
    function explore(v, componentN) {
        nodes[v - 1].visited = true;
        nodes[v - 1].componentNumber = componentN;
        adjList[v - 1].forEach((item) => {
            if (!nodes[item - 1].visited) explore(item, componentN);
        });
        return nodes;
    }
    function Node() {
        this.visited = false;
        this.componentNumber = 0;
    }
    function createNodes () {
        let res = [];
        for (let i = 0; i < vNum; i++){res.push(new Node);}
        return res;
    }
    function makeAdjacentList() {
        let adjList = [];
        for (let i = 0; i < vNum; i++) {
            adjList.push([])
        }
        for (let c = 0; c < edges.length; c++) {
            adjList[edges[c][0] - 1].push(edges[c][1]);
            adjList[edges[c][1] - 1].push(edges[c][0]);
        }
        return adjList;
    }
}

function pathFinder(){
    let gr = new Graph(n, list);
    console.log(gr.componentsSearch());
}