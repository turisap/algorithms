/**
 * Created by HP on 03-Oct-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);

var currentLine = 1;
function helper(line) {
    if(currentLine === 2) {
        var sequence = line.trim().split(' ');
        sequence = sequence.map(function (item) {
           return parseInt(item);
        });
        findTreeDepth(sequence);
        rl.close();
        process.exit();
    }
    if (currentLine === 1) {
        currentLine++;
    }
}

var Node = function (parent = null) {
    this.parent = parent;
    this.children = [];

    this.addChild = function (child) {
        this.children.push(child);
    }
};

function findTreeDepth(tree) {

    this.nodesArray = [];
    this.rootIndex = 0;
    this.init = function () {
        this.createNodeArray();
        this.buildTree();
        this.treeDepth();
    };

    this.createNodeArray = function() {
        for (var i = 0; i < tree.length; i++) {
            var parentIndex = tree[i];
            this.nodesArray.push(new Node(parentIndex));
        }
    };

    this.buildTree = function (){
        for (var c = 0; c < this.nodesArray.length; c++) {
            var parentIndex = this.nodesArray[c].parent;
            if(parentIndex !== -1) {
                this.nodesArray[parentIndex].addChild(c);
            } else {
                this.rootIndex = c;
            }
        }
    };

    this.treeDepth = function () {
        var read = 0;
        var write = 0;
        var queue = new Array(this.nodesArray.length + 1);

        function enqueue(element) {
            queue[write] = element;
            write = (write + 1) % queue.length;
        }
        function dequeue() {
            var dequeued = queue[read];
            queue[read] = null;
            read = (read + 1) % queue.length;
            return dequeued;
        }

        var toQueue = this.nodesArray[this.rootIndex];
        enqueue(toQueue);
        var currentLayer = 1 /*toQueue.children.length*/;
        var nextLayer = 0;
        var layerCounter = 1;


        while (read != write) {
            //console.log(`currentLayer = ${currentLayer}, nextLayer = ${nextLayer}, layerCounter = ${layerCounter}`);
            var dequeued = dequeue();
            if (currentLayer === 0) {currentLayer = nextLayer; nextLayer = 0; layerCounter++;}
            //console.log(dequeued)
            if(dequeued.children.length > 0) {
                //console.log('ca')
                dequeued.children.forEach(function (child) {
                    enqueue(this.nodesArray[child]);
                    nextLayer++;
                });
            }
            currentLayer--;
            //console.log(`currentLayer = ${currentLayer}, nextLayer = ${nextLayer}, layerCounter = ${layerCounter}`);
            //console.log('======');
        }
        console.log(layerCounter)
    };

    this.init();
}