/**
 * Created by HP on 10-Oct-17.
 */
var readline = require('readline');
var bigInt = require('big-integer');
var rl = readline.createInterface(process.stdin, process.stdout);
rl.on('line', assigner);

var currentLine = 1;
var pattern = '';
var text = '';
function assigner(line) {
    if (currentLine === 1) {
        pattern = line.trim();
    }
    if (currentLine === 2) {
        text = line.trim();
        //findPatternsInText(pattern, text);
        textChecker(pattern, text);
        rl.close();
        process.exit();
    }
    currentLine++;
}

function textChecker(pattern, text) {
    var result = [];
    var p = parseInt(bigInt(pattern.length * text.length).pow(2).toString());
    var x = 1 /*Math.floor(Math.random() * p)*/;
    var patternHash = polyHash(pattern, p, x);
    var precomputedHashes = preComputeHashes(text, pattern, p, x);

    for (var i = 0, len = text.length - pattern.length; i <= len; i++) {
        if (precomputedHashes[i] !== patternHash) {continue;}
        if(text.substring(i, i + pattern.length) === pattern) {result.push(i)}
    }

    function preComputeHashes(text, pattern, p, x){
        var lastSubstr = text.substring(text.length - pattern.length, text.length);
        var lstHash = polyHash(lastSubstr, p, x);
        var hashes = [lstHash];
        //console.log(polyHash(pattern, p,x));
        var y = bigInt(1);
        for (var i = 0; i < pattern.length; i++) {
            y = y.multiply(x).mod(p); // may be wrong
        }
        for (i = text.length - pattern.length - 1; i >= 0; i--) {
            hashes.unshift(0);
            hashes[0] = parseInt(((bigInt(hashes[1]).multiply(x).add((parseInt(text.charCodeAt(i)) - y * text.charCodeAt(i + pattern.length)) % p)).add(p)).mod(p).toString());
        }
        return hashes;
    }
    function polyHash(P,p,x) {
        var hash = 0;
        for (var i = 0; i < P.length; i++) {
            var current = bigInt(x).pow(i).multiply(parseInt(P.charCodeAt(i)));
            hash = current.add(hash);
        }
        hash = hash.mod(p);
        return parseInt(hash.toString());
    }
    //preComputeHashes(text, pattern, p, x);
    console.log(result.join(' '))
}


/*   for (var i = 0, len = text.length - pattern.length; i <= len; i++) {
 var currentHash = polyHash(text.substring(i, i + pattern.length), p, x);
 if (currentHash !== patternHash) {continue;}
 if(areEqual(text.substring(i, i + pattern.length), pattern)) {result.push(i)}
 }*/