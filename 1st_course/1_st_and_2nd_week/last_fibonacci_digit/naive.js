/**
 * Created by HP on 09-Sep-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);


function helper(line){
    console.log(lastFibonacciNumber(line));
    process.exit();
}

function lastFibonacciNumber (line) {
    var num = parseInt(line);
    if (checkIfFirstOrSecond(num)) {
        return num;
    }

    var previous = 0;
    var current  = 1;

    for (var i = 0; i < (num - 1); i++) {
        var temporaryPrevious = previous;
        previous  = current;
        current   = current + temporaryPrevious;
    }


    function checkIfFirstOrSecond(number) {
        return (number == 0 || number == 1);
    }

    return current.toString().split('').pop();
}





