/**
 * Created by HP on 13-Oct-17.
 */
//'use strict'
let readline = require('readline');
let rl  = readline.createInterface(process.stdin, process.stdout);
let cl = 1;
let n = 0;
let vertexes = [];

rl.on('line', assigner);

function assigner (line) {
    if (cl === 1) {
        n = parseInt(line);
    }
    if (1 < cl && cl <= n + 1) {
        let element = line.trim().split(' ');
        element = element.map(Number);
        vertexes.push(element);
    }
    if (cl > n) {
        traverseTree();
        rl.close();
        process.exit();
    }
    cl++;
}

function traverseTree() {
    console.log(inOrderTraversal(vertexes[0]));
    console.log(preOrderTraversal(vertexes[0]));
    console.log(postOrderTraversal(vertexes[0]));
}

let resIn = [];
let resPre = [];
let resPost = [];
function inOrderTraversal(tree) {
    if (!tree) return;
    inOrderTraversal(leftCh(tree));
    resIn.push(tree[0]);
    inOrderTraversal(rightCh(tree));
    return resIn.join(' ');
}
function preOrderTraversal(tree) {
    if (!tree) return;
    resPre.push(tree[0]);
    preOrderTraversal(leftCh(tree));
    preOrderTraversal(rightCh(tree));
    return resPre.join(' ');
}
function postOrderTraversal(tree) {
    if (!tree) return;
    postOrderTraversal(leftCh(tree));
    postOrderTraversal(rightCh(tree));
    resPost.push(tree[0]);
    return resPost.join(' ');
}

function leftCh(tree) {
    if (tree[1] === -1) return false;
    return vertexes[tree[1]];
}
function rightCh(tree) {
    if (tree[2] === -1) return false;
    return vertexes[tree[2]];
}