/**
 * Created by HP on 13-Sep-17.
 */
var readline = require('readline');
var bigInt = require('big-integer');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);

function helper(line) {
    var n = bigInt(parseInt(line.split(' ').shift()));
    var m = bigInt(parseInt(line.split(' ').pop()));
    console.log(n.mod(m));
    console.log(m.mod(n));
}