/**
 * Created by HP on 06-Oct-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', assigner);

var currentLine = 1;
var n = 0;
var sequence = [];
function assigner(line) {
    if (currentLine === 1) {
        n = parseInt(line.trim());
    } else {
        sequence = line.trim().split(' ').map(Number);
        MaxHeap(sequence);
        rl.close();
        process.exit();
    }
    currentLine++;
}

var MaxHeap = function (sequence) {
    this.numbersOfSwaps = 0;
    this.swappedPairs = [];
    this.size = sequence.length - 1;

    this.init = function () {
        this.buildMinHeap();
    };

    this.buildMinHeap = function () {
        for (var i = Math.floor(this.size / 2); i >= 0; i--) {
            this.siftDown(i);
        }
        //console.log(sequence);
        console.log(this.numbersOfSwaps);
        this.swappedPairs.forEach(function (pair) {
            console.log(pair.join(' '));
        });
    };

    this.siftDown = function (index) {
        minIndex = index;
        var l = this.leftChild(minIndex);
        var r = this.rightChild(minIndex);

        if (l <= this.size && sequence[l] < sequence[minIndex]) {
            minIndex = l;
        }
        if (r <= this.size && sequence[r] < sequence[minIndex]) {
            minIndex = r;
        }
        if (index != minIndex) {
            this.swap(index, minIndex);
            this.siftDown(minIndex);
        }
    };
    this.swap = function (indexA, indexB) {
        var temporaryElement = sequence[indexA];
        sequence[indexA] = sequence[indexB];
        sequence[indexB] = temporaryElement;
        this.numbersOfSwaps++;
        this.swappedPairs.push([indexA, indexB]);
    };
    this.parent = function (i) {
        return Math.floor((i - 1) / 2);
    };
    this.leftChild = function (i) {
        return (2 * i) + 1;
    };
    this.rightChild = function (i) {
      return (2 * i) + 2;
    };

    this.init();
};