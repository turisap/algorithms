/**
 * Created by HP on 14-Oct-17.
 */

var readline = require('readline');
var rl  = readline.createInterface(process.stdin, process.stdout);
var cl = 1;
var n = 0;
var vertexes = [];

rl.on('line', assigner);

function assigner (line) {
    if (cl === 1) {
        n = parseInt(line);
    }
    if (1 < cl && cl <= n + 1) {
        var element = line.trim().split(' ');
        element = element.map(Number);
        vertexes.push(element);
    }
    if (cl > n) {
        console.log(traverseTree() ? 'CORRECT' : 'INCORRECT');
        rl.close();
        process.exit();
    }
    cl++;
}

function Tree (tree) {
    var treeM = tree;
    this.inOrderStack = [];
    this.inOrderTraversal = function (tree) {
        if (tree === false) return;
        this.inOrderTraversal(getLeftChild(tree));
        this.inOrderStack.push(tree[0]);
        this.inOrderTraversal(getRightChild(tree));
        return this.inOrderStack;
    };
    function getLeftChild(tree) {
        if (tree[1] === -1) return false;
        return treeM[tree[1]];
    }
    function getRightChild(tree) {
        if (tree[2] === -1) return false;
        return treeM[tree[2]];
    }
}

function traverseTree() {
    if (vertexes.length === 0) return true;
    var t = new Tree(vertexes);
    var st = t.inOrderTraversal(vertexes[0]);
    //console.log(st)
    for (var i = 1; i < st.length; i++) {
        if (st[i] < st[i - 1]) return false;
    }
    return true;
}