/**
 * Created by HP on 13-Sep-17.
 */

var bigInt = require('big-integer');

while (true) {
    var n = bigInt(Math.ceil(Math.random() * 10000));

    var fastSolution = lastDigitOfFibonacciSumFast(n);
    var naiveSolution = lastDigitOfFibonacciSumNaive(n);

    if (naiveSolution != fastSolution) {
        console.log('\n\n\n');
        console.log(`WRONG ANSWER!!! N: ${n.toString()}; Fast solution: ${fastSolution}, naive solution: ${naiveSolution}`);
        console.log('\n\n\n');
        break;
    }
    console.log('================= NEW ROW ==================');
    console.log(`OK N: ${n} ${fastSolution} = ${naiveSolution}`);
}

function lastDigitOfFibonacciSumNaive(n) {
    if (n.toString() <= 1) {
        return n.toString();
    }

    var previous = bigInt(0);
    var current  = bigInt(1);
    var sum      = bigInt(1);
    var loopLimit = parseInt(n.toString() - 1);
    //console.log(`\n\n\n ${parseInt(n.toString() - 1)} \n\n\n`);

    for (var i = 0; i < loopLimit; i++) {
        var tempPrevious = previous;
        previous = current;
        current  = current.add(tempPrevious);
        sum = sum.add(current);
    }
    return (sum.mod(10)).toString();
}

function lastDigitOfFibonacciSumFast(n) {
    if (checkIfFirstOrSecond(parseInt(n.toString()))) {
        return n.toString();
    }

    var lastDigits    = [bigInt(0), bigInt(1)];
    var sequenceFound = false;
    var zeroFound     = false;
    var counter       = 2;

    while (! sequenceFound) {

        var currentLastDigit = (lastDigits[counter - 1].add(lastDigits[counter - 2])).mod(10);
        lastDigits.push(currentLastDigit);

        if (zeroFound && currentLastDigit.toString() == 1) {
            sequenceFound = true;
        }
        if (zeroFound && currentLastDigit.toString() != 1) {
            zeroFound = false;
        }
        if (currentLastDigit.toString() == 0) {
            zeroFound = true;
        }
        counter++;
    }

    lastDigits.pop();
    lastDigits.pop();

    var index = n.mod(lastDigits.length);
    var sum = 0;
    for (var i = 0; i < lastDigits.length; i++) {
        sum += parseInt(lastDigits[i].toString());
    }

    for (var j = 0; j <= index; j++) {
        sum += parseInt(lastDigits[j].toString());
    }

    return sum % 10;

}

function checkIfFirstOrSecond(n) {
    return (n === 0 || n === 1);
}


