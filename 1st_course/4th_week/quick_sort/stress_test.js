/**
 * Created by HP on 27-Sep-17.
 */
(function generator() {
    this.init = function () {
        this.looper();
    };

    this.looper = function () {
        while (true) {
            var sequence = this.getRandomSequence();

            var fastSolution = quickSort3Partition(sequence, 0, sequence.length - 1);
            var slowSolution = quickSort2Partition(sequence, 0, sequence.length - 1);

            if (fastSolution != slowSolution) {
                console.log('ERROR!!!!');
                console.log(`FastSolution: ${fastSolution}`);
                console.log(`SlowSolution: ${slowSolution}`);
                break;
            }
            console.log('OK');
            console.log(`${fastSolution} === ${slowSolution}`);
        }
    };

    this.getRandomSequence = function () {
        var randomLength = Math.floor(Math.random() * 100 + 1);
        var sequence = [];

        for (var i = 0; i < randomLength; i++) {
            var randomValue = Math.floor(Math.random() * 3 + 1);
            sequence.push(randomValue);
        }
        return sequence;
    };

    this.init();
})();




function quickSort3Partition (array, l, r) {

    if (l < r) {
        var borders = partition3(array, l, r);
        var lowBorder = borders.shift();
        var highBorder = borders.pop();
        quickSort3Partition(array, l, lowBorder - 1);
        quickSort3Partition(array, highBorder + 1, r);
    }
    return array.join(' ');
}

function swap(array, leftIndex, rightIndex) {
    var temp = array[leftIndex];
    array[leftIndex] = array[rightIndex];
    array[rightIndex] = temp;
}

function partition3 (array, left, right) {
    //console.log(array);
    var randomElementIndex = Math.floor(Math.random() * (right - left) + left);
    swap(array, randomElementIndex, left);
    var pivot = left;
    var j = left;

    //console.log(randomElementIndex);

    for (var counter = left + 1; counter <= right; counter++) {
        if (array[counter] <= array[pivot]) {
            j++;
            swap(array, j, counter);
        }
    }
    swap(array, j, pivot);
    //console.log(array);
    //console.log(`j = ${j}`);


    var newPlace = j - 1;
    for (var i = j - 1; i >= 0; i--) {
        if (array[i] === array[j]) {
            swap(array, i, newPlace);
            newPlace--;
        }
    }
    newPlace++;
    //console.log(array);

    return [newPlace , j];

}


/* ====================== NAIVE SOLUTION ========================= */
function quickSort2Partition(sequence, l, r) {
    if (l < r) {
        var m = partition(sequence, l, r);
        quickSort2Partition(sequence, l, m - 1);
        quickSort2Partition(sequence, m + 1, r);
    }
    return sequence.join(' ');
}

function partition (array, left, right) {
    var pivot = left;
    var j = left;

    for (var counter = left + 1; counter <= right; counter++) {
        if (array[counter] <= array[pivot]) {
            j++;
            swap(array, j, counter);
        }
    }
    swap(array, j, pivot);
    return j;
}