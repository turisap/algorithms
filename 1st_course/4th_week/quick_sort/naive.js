/**
 * Created by HP on 27-Sep-17.
 */

var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);

var currentLine = 1;
var sequenceLength = 0;
var sequence = [];

function helper(line) {
    if (currentLine === 1) {
        sequenceLength = parseInt(line);
    }
    if (currentLine === 2) {
        sequence = line.split(' ');
        sequence = sequence.map(function (item) {
            return parseInt(item);
        });
        console.log(quickSort2Partition(sequence, 0, sequenceLength - 1));
        rl.close();
        process.exit();
    }
    currentLine++;
}

function quickSort2Partition(sequence, l, r) {
    if (l < r) {
        var m = partition(sequence, l, r);
        quickSort2Partition(sequence, l, m - 1);
        quickSort2Partition(sequence, m + 1, r);
    }
    return sequence.join(' ');
}

function swap(array, leftIndex, rightIndex) {
    var temp = array[leftIndex];
    array[leftIndex] = array[rightIndex];
    array[rightIndex] = temp;
}

function partition (array, left, right) {
    var pivot = left;
    var j = left;

    for (var counter = left + 1; counter <= right; counter++) {
        if (array[counter] <= array[pivot]) {
            j++;
            swap(array, j, counter);
        }
    }
    swap(array, j, pivot);
    return j;
}