/**
 * Created by HP on 04-Oct-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', assigner);
var currentLine = 1;
var bufferCapacity = 0;
var packetsNumber = 0;
var timeValues     = [];

function assigner (line) {
    if (currentLine === 1) {
        data = line.trim().split(' ');
        bufferCapacity = parseInt(data[0]);
        packetsNumber = parseInt(data[1]);
    }
    if (currentLine > 1) {
        timeValues.push(line.trim().split(' '));
    }
    if (currentLine == packetsNumber + 1) {
        var result = processor(timeValues, bufferCapacity);
        //console.log('==========================')
        if (Array.isArray(result)) {
            result.forEach(function (res) {
                console.log(res);
            })
        } else {
            console.log(result)
        }
        rl.close();
        process.exit();
    }
    currentLine++;
}

function processor(values, bufferCapacity) {
    var resultingTime = [];

    this.init = function () {
        this.processor(this.reformatPackets(values));
    };

    this.processor = function (packages) {
        var currentTime = 0;
        var finishCurrentExecution = 0;
        var executionTime = ((packages.slice(-1)).pop()).arrivalTime + 1;
        var read = 0;
        var write = 0;
        var buffer = new Array(bufferCapacity + 1);

        function enqueue(element) {
            buffer[write] = element;
            write = (write + 1) % buffer.length;
        }
        function dequeue() {
            var dequeued = buffer[read];
            buffer[read] = 'empty';
            read = (read + 1) % buffer.length;
        }

        while (currentTime <= executionTime) {
            // process packs from buffer
            for (var j = 0; j < buffer.length; j++) {
                if (buffer[j] <= currentTime) {
                    dequeue();
                }
            }
            //console.log(buffer);
            // check packages whether there are packs to process
            var toDelete = 0;
            for (var i = 0; i < packages.length; i++) {
                var currentPack = packages[i];
                if (currentPack.arrivalTime === currentTime) {
                    if (bufferCanAccept()) {
                        if (computerIsFree()) {
                            resultingTime.push(currentTime);
                        } else {
                            resultingTime.push(finishCurrentExecution);
                        }
                        finishCurrentExecution += currentPack.processingTime;
                        if (currentPack.processingTime > 0) {enqueue(finishCurrentExecution);}
                    } else {resultingTime.push(-1)}
                    toDelete++;
                } else {break;}
            }

            for (toDelete; toDelete > 0; toDelete--) {
                packages.shift();
            }

            currentTime++;
        }

        function computerIsFree() {
            return currentTime >= finishCurrentExecution;
        }
        function bufferCanAccept(){
            var freeSpaces = 0;
            for (var d = 0; d < buffer.length; d++){
                if(isNaN(buffer[d])){freeSpaces++}
            }
            //console.log(`read = ${read}, write = ${write}, buffer = ${buffer} bufferCanAccept = ${freeSpaces > 1}`)
            return freeSpaces > 1;
        }


        function getSimultaneousPackages(nextArrival) {
            var simultaneousPacks = [];
            var counter = 0;
            for (var i = 0; i < packages.length; i++) {
                if (packages[i].arrivalTime === nextArrival) {simultaneousPacks.push(packages[i]); counter++}
                if (packages[i].arrivalTime != nextArrival) {break;}
            }
            for (var c = 0; c < counter; c++) {
                packages.shift();
            }
            return simultaneousPacks;
        }
    };
    /* cd xampp/htdocs/courses/algorithms/data_structures/1st_week/packets_process */
    this.reformatPackets = function (values) {
        var Package = function (valuePair) {
            this.arrivalTime = parseInt(valuePair.shift());
            this.processingTime = parseInt(valuePair.shift());
        };
        var packagesForProcessing = [];

        values.forEach(function(value) {
            packagesForProcessing.push(new Package(value));
        });
        return packagesForProcessing;
    };
    this.init();
    return resultingTime;
}