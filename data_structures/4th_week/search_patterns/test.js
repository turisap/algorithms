/**
 * Created by HP on 10-Oct-17.
 */
var bigInt = require('big-integer');
(function generator() {

    while (true) {
        var patternLength = Math.floor(Math.random() + 100);
        var textLength    = Math.floor(Math.random() * 100 + 2);
        var possible      = 'q';
        var pattern = [];
        var text = [];

        for (var i = 0; i < patternLength; i++) {
            pattern.push(possible.charAt(Math.floor(Math.random() * possible.length)));
        }
        for (var c = 0; c < textLength; c++) {
            text.push(possible.charAt(Math.floor(Math.random() * possible.length)));
        }

        pattern = pattern.join('');
        text = text.join('');
        var p = parseInt(bigInt(pattern.length * text.length).pow(2).toString() + 1);
        var x = Math.floor(Math.random() * (p - 1) + 1);

        var naiveSolution = textCheckerNaive(pattern, text, p, x);
        var fastSolution  = textChecker(pattern, text, p, x);

        if (naiveSolution != fastSolution) {
            console.log('\n');
            console.log('ERROR!!!');
            console.log(`text length = ${text.length}, pattern length = ${pattern.length}`)
            console.log(`text = ${text}, pattern = ${pattern}`)
            console.log(`Fast solution = ${fastSolution}, slow solution = ${naiveSolution}`);
            console.log(`p = ${p}, x = ${x}`)
            break;
        }

        console.log('OK');
        console.log(`p = ${p}, x = ${x}`)
        console.log(`Fast solution = ${fastSolution}, slow solution = ${naiveSolution}`);
        console.log('====================================');
    }

})();



function textCheckerNaive(pattern, text, p, x) {
    var result = [];
    //var p = parseInt(bigInt(pattern.length * text.length).pow(2).toString() + 1);
    var patternHash = polyHash(pattern, p, x);
    for (var i = 0, len = text.length - pattern.length; i <= len; i++) {
        var currentHash = polyHash(text.substring(i, i + pattern.length), p, x);
        if (currentHash !== patternHash) {continue;}
        if(areEqual(text.substring(i, i + pattern.length), pattern)) {result.push(i)}
    }

    function polyHash(P,p,x) {
        var hash = 0;
        for (var i = 0; i < P.length; i++) {
            var current = bigInt(x).pow(i).multiply(parseInt(P.charCodeAt(i)));
            hash = current.add(hash);
        }
        hash = hash.mod(p);
        return parseInt(hash.toString());
    }
    function areEqual(substring, pattern) {
        if (substring.length != pattern.length) {
            return false;
        }
        for (var i = 0, len = pattern.length; i < len; i++) {
            if (substring.charAt(i) !== pattern.charAt(i)) {
                return false;
            }
        }
        return true;
    }
    return (result.join(' '));
}
function textChecker(pattern, text, p, x) {
    var result = [];
    //var p = parseInt(bigInt(pattern.length * text.length).pow(2).toString() + 1);
    //var x = Math.floor(Math.random() * (p - 1) + 1);
    var patternHash = polyHash(pattern, p, x);
    var precomputedHashes = preComputeHashes(text, pattern, p, x);

    for (var i = 0, len = text.length - pattern.length; i <= len; i++) {
        if (precomputedHashes[i] !== patternHash) {continue;}
        if(areEqual(text.substring(i, i + pattern.length), pattern)) {result.push(i)}
    }

    function preComputeHashes(text, pattern, p, x){
        var lastSubstr = text.substring(text.length - pattern.length, text.length);
        var lstHash = polyHash(lastSubstr, p, x);
        var hashes = [lstHash];
        //console.log(polyHash(pattern, p,x));
        var y = bigInt(1);
        for (var i = 0; i < pattern.length; i++) {
            y = y.multiply(x).mod(p); // may be wrong
        }
        for (i = text.length - pattern.length - 1; i >= 0; i--) {
            hashes.unshift(0);
            hashes[0] = parseInt(((bigInt(hashes[1]).multiply(x).add((parseInt(text.charCodeAt(i)) - y * text.charCodeAt(i + pattern.length)) % p)).add(p)).mod(p).toString());
        }
        console.log()
        return hashes;
    }
    function polyHash(P,p,x) {
        var hash = 0;
        for (var i = 0; i < P.length; i++) {
            var current = bigInt(x).pow(i).multiply(parseInt(P.charCodeAt(i)));
            hash = current.add(hash);
        }
        hash = hash.mod(p);
        return parseInt(hash.toString());
    }
    function areEqual(substring, pattern) {
        /*if (substring === pattern) {return true;}
         return false;*/
        if (substring.length != pattern.length) {
            return false;
        }
        for (var i = 0, len = pattern.length; i < len; i++) {
            if (substring.charAt(i) !== pattern.charAt(i)) {
                return false;
            }
        }
        return true;
    }
    //preComputeHashes(text, pattern, p, x);
    return (result.join(' '));
}