/**
 * Created by HP on 13-Sep-17.
 */
var readline = require('readline');
var bigInt = require('big-integer');

var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);

function helper(line) {
    var n = bigInt(line.split(' ').shift());
    var m = parseInt(line.split(' ').pop());

    console.log(fibonacciModulesFast(n, m));

    rl.close();
    process.exit();
}


function fibonacciModulesFast(n, m) {

    if (checkIfFirstOrSecond(n.toString())) {
        return n;
    }
    if (m === 1 ) {
        return 0;
    }

    var pisanoPeriod = [bigInt(0), bigInt(1)];
    var sequenceFound = false;
    var zeroFound     = false;
    var counter = 2;

    while(! sequenceFound) {

        var currentPerion = (pisanoPeriod[counter - 1].add(pisanoPeriod[counter - 2])).mod(m);
        pisanoPeriod.push(currentPerion);

        if (zeroFound && currentPerion.toString() == 1) {
            sequenceFound = true;
        }
        if (zeroFound && currentPerion.toString() != 1) {
            zeroFound = false;
        }
        if (currentPerion.toString() == 0) {
            zeroFound = true;
        }

        counter++;
    }

    pisanoPeriod.pop();
    pisanoPeriod.pop();

    var index = n.mod(pisanoPeriod.length);

    return pisanoPeriod[index].toString();

}

function checkIfFirstOrSecond(n) {
    return (n === 0 || n === 1);
}