/**
 * Created by HP on 15-Sep-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);
var currentLine = 1;
var n = 0;

rl.on('line', helper);

function helper(line) {

    if (currentLine === 1) {
        n = parseInt(line);
    }
    if (currentLine === 2) {
        var sequence = line.split(' ').map(Number);

        makeMeWellPaid(n, sequence);

        rl.close();
        process.exit();
    }
    currentLine++;
}

function makeMeWellPaid(n, sequence) {
    var answerString = '';

    while (sequence.length > 0) {
        var currentMax = sequence[0];

        for (var i = 1; i < sequence.length; i++) {
            if(greaterOrEqual(sequence[i], currentMax)) {
                currentMax = sequence[i];
            }
        }

        answerString += currentMax.toString();
        //answerString += ' ';
        sequence.splice(sequence.indexOf(currentMax), 1);
        //console.log(sequence);
    }
    console.log(answerString);
}

function greaterOrEqual(iteration, currentMax) {
    var singleDigitNumbers = iteration.toString().split('');
    var currentMaximum = currentMax.toString().split('');

    //console.log(singleDigitNumbers, currentMaximum);

    for (var c = 0; c < singleDigitNumbers.length && c < currentMaximum.length; c++) {
        var iterationDigit = parseInt(singleDigitNumbers[c]);
        var maximumDigit   = parseInt(currentMaximum[c]);

        if (iterationDigit !== maximumDigit) {
            return parseInt(singleDigitNumbers[c]) >=  parseInt(currentMaximum[c]);
        }

        if((iterationDigit === maximumDigit) && (singleDigitNumbers.length === currentMaximum.length)) {
            continue;
        }

        if((iterationDigit === maximumDigit) && (singleDigitNumbers.length !== currentMaximum.length)) {
            //console.log(singleDigitNumbers, currentMaximum);
            return checkUnequalNumbers(singleDigitNumbers, currentMaximum);
        }

    }

}

function checkUnequalNumbers(iteration, currentMax) {

    var iterationClone = iteration.slice(0);
    var currentMaxClone = currentMax.slice(0);

    while (iteration.length > 0 && currentMax.length > 0) {
        var iterationHigh = parseInt(iteration.shift());
        var maximumHigh   = parseInt(currentMax.shift());

        /*console.log(iteration);
        console.log(currentMax);*/

        if (iterationHigh != maximumHigh) {
           return iterationHigh > maximumHigh;
        }
    }

    return stripAndCheck();

    function stripAndCheck() {

        if (iteration.length > currentMax.length) {
            //console.log('teration longer')
            for (var j = 0; j < iteration.length; j++) {
                if (iteration[j] === currentMaxClone[0]) {
                    continue;
                }
                return iteration[j] > currentMaxClone[0];
            }
            return iterationClone[0] > iterationClone[1]; // don't change
        }
        if (currentMax.length > iteration.length) {
             //console.log('maximum longer');
            for (var l = 0; l < currentMax.length; l++) {
                if(currentMax[l] === iterationClone[0]) {
                    continue;
                }
                return currentMax[l] < iterationClone[0];
            }
            return currentMaxClone[0] < currentMaxClone[1];  // don't change*/
        }
    }

}