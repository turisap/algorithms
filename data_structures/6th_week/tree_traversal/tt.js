/**
 * Created by HP on 13-Oct-17.
 */
//'use strict'
let readline = require('readline');
let rl  = readline.createInterface(process.stdin, process.stdout);
let cl = 1;
let n = 0;
let vertexes = [];

rl.on('line', assigner);

function assigner (line) {
    if (cl === 1) {
        n = parseInt(line);
    }
    if (1 < cl && cl <= n + 1) {
        let element = line.trim().split(' ');
        element = element.map(Number);
        vertexes.push(element);
    }
    if (cl > n) {
        traverseTree();
        rl.close();
        process.exit();
    }
    cl++;
}


let _tree = new WeakMap();
const getRightChild = Symbol('getRightChild');
const getLeftChild  = Symbol('getLeftChild');
const inOrderTraversalReal = Symbol('inOrderTraversalReal');
const preOrderTraversalReal = Symbol('preOrderTraversalReal');
const postOrderTraversalReal = Symbol('postOrderTraversalReal');

class Tree {
    constructor(tree) {
        this.inOrderStack = [];
        this.preOrderStack = [];
        this.postOrderStack = [];
        _tree.set(this, vertexes);
    }

    inOrderTraversal () {
        return this[inOrderTraversalReal](_tree.get(this)[0]);
    }
    preOrderTraversal (){
        return this[preOrderTraversalReal](_tree.get(this)[0]);
    }
    postOrderTraversal() {
        return this[postOrderTraversalReal](_tree.get(this)[0]);
    }

    [inOrderTraversalReal](tree) {
        //console.log(tree)
        if (tree === null) return;
        this[inOrderTraversalReal](this[getLeftChild](tree));
        this.inOrderStack.push(tree[0]);
        this[inOrderTraversalReal](this[getRightChild](tree));
        return this.inOrderStack;
    }
    [preOrderTraversalReal](tree) {
        if (tree === null || tree === undefined) return;
        this.preOrderStack.push(tree[0]);
        this[preOrderTraversalReal](this[getLeftChild](tree));
        this[preOrderTraversalReal](this[getRightChild](tree));
        return this.preOrderStack;
    }
    [postOrderTraversalReal](tree) {
        if (tree === null || tree === undefined) return;
        this[postOrderTraversalReal](this[getLeftChild](tree));
        this[postOrderTraversalReal](this[getRightChild](tree));
        this.postOrderStack.push(tree[0]);
        return this.postOrderStack;
    }

    [getRightChild](tree){
        if (tree[2] === -1) return null;
        return _tree.get(this)[tree[2]];
    }
    [getLeftChild](tree){
        if (tree[1] === -1) return null;
        //console.log(tree[1]);
        return _tree.get(this)[tree[1]];
    }
}

function traverseTree() {
    if (vertexes.length === 0) return;
    let t = new Tree(vertexes);
    console.log(t.inOrderTraversal().join(' '));
    console.log(t.preOrderTraversal().join(' '));
    console.log(t.postOrderTraversal().join(' '));
}
