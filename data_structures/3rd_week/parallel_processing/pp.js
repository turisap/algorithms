/**
 * Created by HP on 07-Oct-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);
rl.on('line', assigner);

var currentLine = 1;
var n = 0;
var sequence = [];
function assigner(line) {
    if (currentLine === 1){
        var data = line.trim().split(' ');
        n = parseInt(data.shift());
    } else {
        sequence = line.trim().split(' ').map(Number);
        var results = processFiles(n, sequence);
        results.forEach(function (item) {
            console.log(item.join(' '))
        });
        rl.close();
        process.exit();
    }
    currentLine++;
}

function FinishingTime(time, thread) {
    this.finishingTime = time;
    this.workingThread = thread;
}

function PriorityQueue (sequence)  {
    this.size = sequence.length - 1;
    this.elementsNow = function () {
        return sequence.length;
    };
    this.insert = function (element) {
        if (sequence.length === this.size) {console.log('error inserting in the queue. capacity overflow');}
        var newLength = sequence.push(element);
        siftUp(newLength - 1);
        //console.log(sequence);
    };
    this.extractMin = function () {
        var result = sequence[0];
        sequence[0] = sequence[sequence.length - 1];
        sequence.pop();
        siftDown(0);
        return result;
    };
    this.isEmpty = function (){
        return sequence.length === 0;
    };
    this.isNotEmpty = function () {
        return sequence.length !== 0;
    };
    function siftUp (i){
        //console.log(`Parent = ${sequence[parent(i)]}, element = ${sequence[i]}`);
        while (i > 0 && sequence[parent(i)] > sequence[i]) {
            swap(parent(i), i);
            i = parent(i);
        }
        //console.log(sequence)
    }
    function siftDown (index) {
        var minIndex = index;
        var l = leftChild(minIndex);
        var r = rightChild(minIndex);

        if (l <= sequence.length - 1 && sequence[l] < sequence[minIndex]) {
            minIndex = l;
        }
        if (r <= sequence.length - 1 && sequence[r] < sequence[minIndex]) {
            minIndex = r;
        }
        if (index != minIndex) {
            swap(index, minIndex);
            siftDown(minIndex);
        }
    }
    function swap(indexA, indexB) {
        var temporaryElement = sequence[indexA];
        sequence[indexA] = sequence[indexB];
        sequence[indexB] = temporaryElement;
    }
    function parent(i) {
        return Math.floor((i - 1) / 2);
    }
    function leftChild(i) {
        return (2 * i) + 1;
    }
    function rightChild(i) {
        return (2 * i) + 2;
    }
}

function PriorityQueueForObjects(heap) {
    this.size = heap.length - 1;
    this.insert = function (element) {
        if (heap.length === this.size ) {console.log('ERROR INSERTING');}
        var newIndex = heap.push(element) - 1;
        siftUp(newIndex);
    };
    this.siftUp = function (i) {
        while (i > 0) {
            if (heap[i].finishingTime < heap[parent(i)].finishingTime) {
                swap(parent(i), i);
                i = parent(i);
            } else if(finishingTimesAreEqualOnDifferentTreads()) {
                swap(parent(i), i);
                i = parent(i);
            } else {
                break;
            }

        }
        function finishingTimesAreEqualOnDifferentTreads() {
            return heap[i].finishingTime === heap[parent(i)].finishingTime && heap[i].workingThread < heap[parent(i)].workingThread;
        }
    };
    function parent(i) {
        return Math.floor((i - 1) / 2);
    }
    function leftChild(i) {
        return (2 * i) + 1;
    }
    function rightChild(i) {
        return (2 * i) + 2;
    }
}


function processFiles(n, jobs) {
    var threads = new PriorityQueue(Array.apply(null, {length: n}).map(Function.call, Number));
    var results = [];
    var finishingTimes = [];
    var currentTime = 0;

    for (var i = 0; i < jobs.length; i++) {
        if(threads.isNotEmpty()) {
            var workingThread = threads.extractMin();
            finishingTimes.push(new FinishingTime(currentTime + jobs[i], workingThread));
            results.push([workingThread, currentTime]);
        } else {
            var nextFinishingTime = getNearestFinishingTime();
            //console.log(nextFinishingTime);
            currentTime = nextFinishingTime.finishingTime;
            //console.log(currentTime)
            finishingTimes.push(new FinishingTime(currentTime + jobs[i], nextFinishingTime.workingThread));
            results.push([nextFinishingTime.workingThread, currentTime]);
        }
        //console.log(finishingTimes);
    }
    //console.log(results);

    function getNearestFinishingTime() {
        var index  = 0;
        var minTime = finishingTimes[index].finishingTime;
        for (var c = 0; c < finishingTimes.length; c++) {
            if (finishingTimes[c].finishingTime < minTime) {
                minTime = finishingTimes[c].finishingTime;
                index = c;
            }
        }
        return finishingTimes.splice(index, 1)[0];
    }
    return results;
}