/**
 * Created by HP on 08-Sep-17.
 */
var isEven = function(n) {
    return n % 2 === 0;
};

var isOdd = function(n) {
    return !isEven(n);
};

var power = function(x, n) {
    console.log("Computing " + x + " raised to power " + n + ".");
    if (n === 0) {
        return 1;
    }

    if (n < 0) {
        return 1 / power(x, (Math.abs(n)));
    }

    if (isOdd(n)) {
        return power(x, (n - 1)) * x;
    }

    if (isEven(n)) {
        var half = power(x, (n / 2));
        return half * half;
    }
};

var displayPower = function(x, n) {
    console.log(x + " to the " + n + " is " + power(x, n));
};

displayPower(3, 0);
displayPower(3, 1);
displayPower(3, 2);
displayPower(3, -1);
displayPower(5, 2);
displayPower(5, -1);