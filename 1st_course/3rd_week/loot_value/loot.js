/**
 * Created by HP on 14-Sep-17.
 */
var readline = require('readline');
var rl       = readline.createInterface(process.stdin, process.stdout);

var lines    = 0;
var items    = 0;
var capacity = 0;
var values   = [];
var weights  = [];

rl.on('line', helper);

function helper(line) {

    if (lines === 0) {
        items = parseInt(line.split(' ').shift());
        capacity = parseInt(line.split(' ').pop());
    }

    if (lines > 0) {
        values.push(parseInt(line.split(' ').shift()));
        weights.push(parseInt(line.split(' ').pop()));
    }

    if (lines == items) {
        console.log(stealMaxValue(items, capacity, values, weights));
        rl.close();
        process.exit();
    }

    lines++;
}

function stealMaxValue(items, capacity, values, weights) {

    // create an array with value/weight grade
    var arrayRatios = getRatiosArray(values, weights);

    var maxValue = 0;
    var cap = capacity;

    while (cap > 0) {

        var largestRatio = Math.max.apply(Math, arrayRatios);
        var largestIndex = arrayRatios.indexOf(largestRatio);
        var itemWeight   = weights[largestIndex];
        var itemValue    = values[largestIndex];

        if (cap > itemWeight) {
            maxValue += itemValue;
            cap -= itemWeight;
        } else {
            maxValue += arrayRatios[largestIndex] * cap;
            cap = 0;
        }
        arrayRatios.splice(largestIndex, 1);
        values.splice(largestIndex, 1);
        weights.splice(largestIndex, 1);

        /*console.log(largestIndex);
        console.log(values);
        console.log(weights);*/
    }
    setEverythingTozero();
    return maxValue;
}

function getRatiosArray (values, weights) {
    var arrayRatios = [];
    for (var i = 0; i < values.length; i++) {
        arrayRatios.push(values[i] / weights[i]);
    }
    return arrayRatios;
}

function setEverythingTozero(){
    lines    = 0;
    items    = 0;
    capacity = 0;
    values   = [];
    weights  = [];
}