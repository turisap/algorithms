#python3
import sys
import math

def binary_search(a, x):
    #check for unsorted array first
    '''for index, element in enumerate(item):
        if index == 0:continue
        if a[index] >= a[index + 1]:
            return false
    
    #check for values less than 1
    if x < 1:
        return -1'''
    
    left, right = 0, len(a) - 1
    
    while right >= left:
        guess = int(math.floor((left + right) / 2))
        #print(guess)
        if a[guess] == x:
            return guess
        elif a[guess] < x:
            left = guess + 1
        elif a[guess] > x:
            right = guess - 1
    return -1


if __name__ == '__main__':
    input = sys.stdin.read()
    data = list(map(int, input.split()))
    n = data[0]
    m = data[n + 1]
    a = data[1 : n + 1]
    for x in data[n + 2:]:
        # replace with the call to binary_search when implemented
        print(binary_search(a, x), end=' ')
