/**
 * Created by HP on 15-Oct-17.
 */
let readline = require('readline');
//let async = require('async');
let rl = readline.createInterface(process.stdin, process.stdout);
let cl = 1;
let n = 0;
let m = 0;
let list = [];
let reversedList = [];

rl.on('line', assigner);

function assigner(line) {
    if (cl === 1) {
        let data = line.trim().split(' ');
        n = parseInt(data[0]);
        m = parseInt(data[1]);
    }
    if (1 < cl && cl <= m + 1) {
        list.push(line.trim().split(' ').map(function(item) {
            return parseInt(item) - 1;
        }));
        reversedList.push(line.trim().split(' ').reverse().map(function (item) {
            return parseInt(item) - 1;
        }))
    }
    if (cl === m + 1) {
        topologicalSort();
        rl.close();
        process.exit();
    }
    cl++;
}

function Graph(vNum, edges, revEdges) {
    let adjList =  makeAdjacentList(edges);
    let revList =  makeAdjacentList(revEdges);
    let nodes   = createNodes();
    let revNodes = createNodes();
    let visitCounter = 1;
    let largestPost = 0;

    this.componentsSearch = function () {  // only for undirected graphs
        let cN = 1;
        for (let i = 0; i < vNum; i++) {
            if (nodes[i].visited) continue;
            explore(i, cN, nodes, adjList);
            cN++;
        }
        //console.log(nodes);
        return (cN - 1);
    };
    this.stronglyCC = function () {
        let cN = 1;
        let cSCC = 1;
        for (let i = 0; i < vNum; i++) {
            if (revNodes[i].visited) continue;
            explore(i, cN, revNodes, revList);
            cN++;
        }
        revNodes.sort(function (a, b) {
            return a.postVisit === b.postVisit ? 0 : (a.postVisit > b.postVisit ? 1 : -1)
            //if(parseInt(a.postVisit) > parseInt(b.postVisit))   return 1;
            //if(parseInt(a.postVisit) < parseInt(b.postVisit))   return -1;
        });
        visitCounter = 1;
        for (let j = revNodes.length - 1; j >= 0 ; j--) {
            if (nodes[revNodes[j].number - 1].visited) continue;
            explore(revNodes[j].number - 1, cSCC, nodes, adjList);
            cSCC ++;
        }
        return cSCC - 1;
    };
    function explore(v, componentN, nodes, list) {
        nodes[v].visited = true;
        nodes[v].componentNumber = componentN;
        preVisit(v);
        list[v].forEach((item) => {
            if (!nodes[item].visited){
                explore(item, componentN, nodes, list)
            }
        });
        postVisit(v, nodes);
        return nodes;
    }
    function Node(n) {
        this.number = n;
        this.visited = false;
        this.componentNumber = 0;
        this.preVisit = 0;
        this.postVisit = 0;
    }
    function createNodes () {
        let res = [];
        for (let i = 0; i < vNum; i++){res.push(new Node(i + 1));}
        return res;
    }
    function makeAdjacentList(list) {
        let adjList = [];
        for (let i = 0; i < vNum; i++) {
            adjList.push([])
        }
        for (let c = 0; c < list.length; c++) {
            adjList[list[c][0]].push(list[c][1]);
            //adjList[edges[c][1] - 1].push(edges[c][0]); // this line was fo undirected graphs
        }
        return adjList;
    }
    function preVisit(v){
        nodes[v].preVisit = visitCounter;
        visitCounter++;
    }
    function postVisit(v, nodes) {
        nodes[v].postVisit = visitCounter;
        largestPost = v;
        visitCounter++;
    }
}

function topologicalSort(){
    let gr = new Graph(n, list, reversedList);
    console.log(gr.stronglyCC());
}