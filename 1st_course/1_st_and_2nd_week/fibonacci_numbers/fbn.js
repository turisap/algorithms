/**
 * Created by HP on 09-Sep-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);


function helper(line){
    console.log(fibonacciNumber(line));
    rl.close();
    process.exit();
}

function fibonacciNumber (line) {
    var num = parseInt(line);
    if (checkIfFirstOrSecond(num)) {
        return num;
    }

    var numbers = [];

    for (var i = 0; i < (num + 1); i++) {
        if (checkIfFirstOrSecond(i)) {
            numbers.push(i);
            continue;
        }
        numbers.push(numbers[i - 1] + numbers[i - 2])
    }

    function checkIfFirstOrSecond(number) {
        return (number == 0 || number == 1);
    }

    return numbers[num];
}





