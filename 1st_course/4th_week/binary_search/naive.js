var readline = require('readline');
var bigInt   = require('big-integer');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);

var currentLine = 1;
var searchArray = null;
var searchValues = null;

function helper(line) {

    if (currentLine === 1) {
        searchArray = line.trim().split(' ');
        searchArray.shift();
        //console.log(searchArray);
    }

    if (currentLine === 2) {
        searchValues = line.trim().split(' ');
        searchValues.shift();
        searchValues = searchValues.map(function (item) {
            return bigInt(item);
        });
        searchNaive(searchArray, searchValues);
        rl.close();
        process.exit();
    }
    currentLine++;
}


function searchNaive(searchArray, searchValues) {
    //console.log(searchArray, searchValues);
    var resultsArray = [];

    for (var i = 0; i < searchValues.length; i++) {
        resultsArray.push(findValueInArray(searchValues[i], searchArray));
    }
    console.log(resultsArray.join(' '));
}

function findValueInArray(value, array) {
    for (var c = 0; c < array.length; c++) {
        if (value.compare(array[c]) === 0) {
            return c;
        }
    }
    return -1;
}