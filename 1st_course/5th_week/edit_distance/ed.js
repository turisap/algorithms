/**
 * Created by HP on 02-Oct-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);

var currentLine = 1;
var firstString = '';
var secondString = '';
function helper(line) {
    if(currentLine === 1) {
        firstString = (line.trim().split(''));
        firstString.unshift(0);
    }
    if(currentLine === 2) {
        secondString = line.trim().split('');
        secondString.unshift(0);
        findEditDistance(secondString, firstString);
        rl.close();
        process.exit();
    }
    currentLine ++;
}


function findEditDistance(firstString, secondString) {
    this.init = function () {
        this.evaluateDistance(firstString, secondString)
    };


    this.evaluateDistance = function (firstString, secondString) {
        var resultsTable = [];

        for (var i = 0; i < firstString.length; i++) {
            var row = [];
            resultsTable.push(row);
            for (var j = 0; j < secondString.length; j++) {
                if (i === 0) {resultsTable[i].push(j);continue;}
                if (j === 0) {resultsTable[i][j] = i;continue;}
                var insertion = resultsTable[i - 1][j] + 1;
                var deletion  = resultsTable[i][j - 1] + 1;
                var match     = resultsTable[i - 1][j - 1];
                var mismatch  = resultsTable[i - 1][j - 1] + 1;

                if (firstString[i] === secondString[j]) {
                    resultsTable[i][j] = Math.min(insertion, deletion, match);
                } else {
                    resultsTable[i][j] = Math.min(insertion, deletion, mismatch);
                }
            }
        }
        console.log((resultsTable.pop()).pop());
    };

    this.init();
}