/**
 * Created by HP on 09-Sep-17.
 */

while (true) {
    var randomChoice = Math.ceil(Math.random() * 35);
    console.log(randomChoice);

    var fastAlgo = fibonacciNumber(randomChoice);
    var naiveAlgo = fibonacciNumberNaive(randomChoice);

    if (fastAlgo !== naiveAlgo) {
        console.log('\n\n\n');
        console.log(`Wrong answer!!!! Fast algorithm: ${fastAlgo}; Naive algorithm: ${naiveAlgo}`);
        console.log('\n\n\n');
        break;
    }
    console.log(`Ok, ${fastAlgo} = ${naiveAlgo}`);
}






function fibonacciNumberNaive (n) {
    if (n <= 1) {
        return n;
    }
    return (fibonacciNumberNaive(n - 1) + fibonacciNumberNaive(n - 2));
}

function fibonacciNumber (line) {
    var num = parseInt(line);
    if (checkIfFirstOrSecond(num)) {
        return num;
    }

    var numbers = [];

    for (var i = 0; i < (num + 1); i++) {
        if (checkIfFirstOrSecond(i)) {
            numbers.push(i);
            continue;
        }
        numbers.push(numbers[i - 1] + numbers[i - 2])
    }

    function checkIfFirstOrSecond(number) {
        return (number == 0 || number == 1);
    }

    return numbers[num];
}