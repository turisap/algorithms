/**
 * Created by HP on 10-Sep-17.
 */
var readline = require('readline');
var bigInt   = require('big-integer');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);

function helper(line) {
    intA = parseInt(line.split(' ').shift());
    intB = parseInt(line.split(' ').pop());
    console.log(leastCommonMultiply(intA, intB));
    rl.close();
    process.exit();
}


function leastCommonMultiply(intA, intB) {
    var gcd = greatestCommonDivisor(intA, intB);
    var bigA = bigInt(intA, 10);
    var bigB = bigInt(intB, 10);
    var lcm = bigA.multiply(bigB).divide(bigInt(gcd, 10));
    return lcm.toString();
}

function greatestCommonDivisor(intA, intB) {
    if (intB === 0) {
        return intA;
    }
    var aPraim = intA % intB;
    return greatestCommonDivisor(intB, aPraim);
}
