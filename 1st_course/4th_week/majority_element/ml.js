/**
 * Created by HP on 25-Sep-17.
 */
var readline  = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', majorityElement);

var currentLine = 1;
var sequenceLength = 0;

function majorityElement(line) {
    this.sequence;
    this.init = function (line) {
        this.assigner(line);
    };

    this.assigner = function (line) {
        if (currentLine === 2) {
            this.sequence = line.split(' ');
            this.sequence = this.sequence.map(function (item) {
                return parseInt(item);
            });
            this.sequenceClone = this.sequence.slice(0);
            if (this.findMajorityElement(this.sequence) !== -1) {
                console.log(1);
            } else {
                console.log(0);
            }
            rl.close();
            process.exit();
        }
        if (currentLine === 1) {
            sequenceLength = parseInt(line);
            currentLine++;
        }
    };

    this.findMajorityElement = function (sequence) {
        if (sequence.length === 1) {
            return sequence[0];
        }

        var k = Math.floor(sequence.length / 2);
        var firstPart = sequence.splice(0, k);
        var leftSubArray = this.findMajorityElement(firstPart);
        var rightSubArray = this.findMajorityElement(sequence);

        if (leftSubArray === rightSubArray) {
            return leftSubArray;
        }


        var leftCount = getFrequency(this.sequenceClone, leftSubArray);
        var rightCount = getFrequency(this.sequenceClone, rightSubArray);
        var nOverTwo = Math.floor(this.sequenceClone.length / 2);

        if (rightCount > nOverTwo) {
            return rightSubArray;
        }
        if (leftCount > nOverTwo) {
            return leftSubArray;
        }

        return -1;
    };

    function getFrequency(initialArray, element) {
        var count = 0;
        for (var d = 0; d < initialArray.length; d++) {
            if (initialArray[d] === element) {
                count++;
            }
        }
        return count;
    }

    this.init(line);
}