/**
 * Created by HP on 13-Sep-17.
 */
var readline = require('readline');
var bigInt   = require('big-integer');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);

function helper(line) {
    var n = bigInt(line);
    console.log(lastDigitOfFibonacciSum(n));

    rl.close();
    process.exit();
}

function lastDigitOfFibonacciSum(n) {

    if (checkIfFirstOrSecond(parseInt(n.toString()))) {
        return n.toString();
    }

    var lastDigits    = [bigInt(0), bigInt(1)];
    var sequenceFound = false;
    var zeroFound     = false;
    var counter       = 2;

    while (! sequenceFound) {

        var currentLastDigit = (lastDigits[counter - 1].add(lastDigits[counter - 2])).mod(10);
        lastDigits.push(currentLastDigit);

        if (zeroFound && currentLastDigit.toString() == 1) {
            sequenceFound = true;
        }
        if (zeroFound && currentLastDigit.toString() != 1) {
            zeroFound = false;
        }
        if (currentLastDigit.toString() == 0) {
            zeroFound = true;
        }
        counter++;
    }

    lastDigits.pop();
    lastDigits.pop();

    var index = n.mod(lastDigits.length);
    var sum = 0;
    for (var i = 0; i < lastDigits.length; i++) {
        sum += parseInt(lastDigits[i].toString());
    }

    for (var j = 0; j <= index; j++) {
        sum += parseInt(lastDigits[j].toString());
    }

    return sum % 10;

}

function checkIfFirstOrSecond(n) {
    return (n === 0 || n === 1);
}