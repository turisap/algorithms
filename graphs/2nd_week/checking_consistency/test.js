/**
 * Created by HP on 16-Oct-17.
 */
let async = require('async');
(function generator() {

    while (true) {
        let nodes = Math.floor(Math.random() * 15 + 1);
        let edges = Math.floor(Math.random() * 15 + 1);

        try{
            let list = [];
            for (let i = 0; i < edges; i++) {
                let v = Math.floor(Math.random() * (nodes - 2) + 1);
                let u = Math.floor(Math.random() * (nodes - 2) + 1);
                list.push([v, (v === u ? (u > 1 ? u - 1 : u) : u)]);
            }
            for (let c = 0; c < list.length; c++) {
                if (list[c][0] === list[c][1]){
                    list.splice(c,c);
                    continue;
                }
                for (let j = 0; j < list.length; j++) {
                    if (list[c].join('') === list[j].join('')) {
                        list.splice(c,c);
                    }
                }
            }

            let gr = new Graph(nodes, list);
            let res = gr.topologicalSort();

            console.log(res);
            console.log(nodes);
            console.log(list);

            if (!orderChecker(res, list)) {
                console.log('\n\n\n');
                console.log('WRONG ANSWER!!!!!');
                console.log(`Result: ${res}`);
                console.log(`Number of nodes: ${nodes}`);
                console.log(`List of edges: ${list}`);
                break;
            }
        } catch (e){
            console.log('\n\n\n');
            console.log(e);
            console.log(`Nodes: ${nodes}`);
            console.log('edges  ' + edges);
            break;
        }

    }
    function *range(...args) {
        let [start, end] = args;
        while (start < end) {
            yield start;
            start++
        }
    }
})();

function Graph(vNum, edges) {
    let adjList =  makeAdjacentList();
    let nodes   = createNodes();
    let visitCounter = 1;
    this.topologicalSort = function () {
        this.componentsSearch();
        let order = [...nodes];
        order.sort(function (a, b) {
            //return a.postVisit === b.postVisit ? 0 : (a.postVisit > b.postVisit ? 1 : -1)
            if(parseInt(a.postVisit) > parseInt(b.postVisit))   return 1;
            if(parseInt(a.postVisit) < parseInt(b.postVisit))   return -1;
        });
        order = order.map(function (item) {return item.number;});
        return order.reverse();
    };
    this.isDAG = function () {
        this.componentsSearch();
        let isDAG = true;
        edges.forEach(edge => {
            //console.log(edge);
            if(nodes[edge[0]].postVisit < nodes[edge[1]].postVisit) isDAG = false;
        });
        return isDAG;
    };
    this.componentsSearch = function () {  // it looks like works only for undirected graphs
        let cN = 1;
        for (let i = 0; i < vNum; i++) {
            if (nodes[i].visited) continue;
            explore(i, cN);
            cN++;
        }
        //console.log(nodes);
        return (cN - 1);
    };
    function explore(v, componentN) {
        nodes[v].visited = true;
        nodes[v].componentNumber = componentN;
        preVisit(v);
        /*async.eachOfSeries(adjList[v], function(item){
           if (!nodes[item].visited) explore(item);
        }, function () {
            postVisit(v);
        });*/
        adjList[v].forEach((item) => {
            if (!nodes[item].visited) explore(item, componentN);
        });
        postVisit(v);
        return nodes;
    }
    function Node(n) {
        this.number = n;
        this.visited = false;
        this.componentNumber = 0;
        this.preVisit = 0;
        this.postVisit = 0;
    }
    function createNodes () {
        let res = [];
        for (let i = 0; i < vNum; i++){res.push(new Node(i + 1));}
        return res;
    }
    function makeAdjacentList() {
        let adjList = [];
        for (let i = 0; i < vNum; i++) {
            adjList.push([])
        }
        for (let c = 0; c < edges.length; c++) {
            adjList[edges[c][0]].push(edges[c][1]);
            //adjList[edges[c][1] - 1].push(edges[c][0]); // this line was fo undirected graphs
        }
        return adjList;
    }
    function preVisit(v){
        nodes[v].preVisit = visitCounter;
        visitCounter++;
    }
    function postVisit(v) {
        nodes[v].postVisit = visitCounter;
        visitCounter++;
    }
}

function orderChecker(result, edges) {
    let conform = true;
    edges.forEach(edge => {
        if (result.indexOf(edge[0]) > result.indexOf(edge[1])) {
            conform = false;
        }
    });
    return conform;
}

//orderChecker([1,2,3,4,5], [[1,2], [3,4], [2,1]]);