/**
 * Created by HP on 04-Oct-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', assigner);
var currentLine = 1;
var bufferCapacity = 0;
var packetsNumber = 0;
var timeValues     = [];

function assigner (line) {
    if (currentLine === 1) {
        data = line.trim().split(' ');
        bufferCapacity = parseInt(data[0]);
        packetsNumber = parseInt(data[1]);
    }
    if (currentLine > 1) {
        timeValues.push(line.trim().split(' '));
    }
    if (currentLine == packetsNumber + 1) {
        var result = processor(timeValues, bufferCapacity);
        //console.log('==========================')
        if (Array.isArray(result)) {
            result.forEach(function (res) {
                console.log(res);
            })
        } else {
            console.log(result)
        }
        rl.close();
        process.exit();
    }
    currentLine++;
}

function processor(values, bufferCapacity) {
    var resultingTime = [];

    this.init = function () {
        this.processor(this.reformatPackets(values));
    };

    this.processor = function (packages) {
        var read = 0;
        var write = 0;

        var buffer = new Array(bufferCapacity + 1).fill('empty');
        var finishTime = 0;


        for (var i = 0; i < packages.length; i++) {
            var currentArrival = packages[i].arrivalTime;
            var currentProcessing = packages[i].processingTime;

            cleanBuffer();

            if (currentArrival >= finishTime) { // if package comes after processing a previous one
                resultingTime.push(currentArrival);
                finishTime = currentArrival + currentProcessing;
                if (currentProcessing > 0) {enqueue(finishTime);}
            } else {
                if (bufferCanAccept()) {
                    resultingTime.push(finishTime);
                    finishTime += currentProcessing;
                    enqueue(finishTime);
                } else {
                    resultingTime.push(-1);
                }
            }
        }

        function cleanBuffer() {
            buffer.forEach(function (item) {
                if (! isNaN(item)) {
                    if (item <= currentArrival) {dequeue()}
                }
            })
        }

        function bufferCanAccept(){
            var freeSpaces = 0;
            for (var d = 0; d < buffer.length; d++){
                if(isNaN(buffer[d])){freeSpaces++}
            }
            //console.log(`read = ${read}, write = ${write}, buffer = ${buffer} bufferCanAccept = ${freeSpaces > 1}`)
            return freeSpaces > 1;
        }
        function enqueue(element) {
            buffer[write] = element;
            write = (write + 1) % buffer.length;
        }
        function dequeue() {
            var dequeued = buffer[read];
            buffer[read] = 'empty';
            read = (read + 1) % buffer.length;
        }
    };
    /* cd xampp/htdocs/courses/algorithms/data_structures/1st_week/packets_process */
    this.reformatPackets = function (values) {
        var Package = function (valuePair) {
            this.arrivalTime = parseInt(valuePair.shift());
            this.processingTime = parseInt(valuePair.shift());
        };
        var packagesForProcessing = [];

        values.forEach(function(value) {
            packagesForProcessing.push(new Package(value));
        });
        return packagesForProcessing;
    };
    this.init();
    return resultingTime;
}