/**
 * Created by HP on 17-Oct-17.
 */
let readline = require('readline');
let rl = readline.createInterface(process.stdin, process.stdout);
let cl = 1;
let n = 0;
let list = [];

rl.on('line', assigner);

function assigner(line) {
    if (cl === 1) {
        n = parseInt(line.trim());
    }
    if (1 < cl && cl <= n + 1) {
        list.push(line.trim().split(' ').map(Number));
    }
    if (cl === n + 1) {
        let data = line.trim().split(' ');
        findDistance();
        rl.close();
        process.exit();
    }
    cl++;
}

function PriorityQueue(sequence) {
    this.numbersOfSwaps = 0;
    this.swappedPairs = [];
    this.size = function() {
        return sequence.length - 1;
    };
    this.isInQueue = function (number) {
        for (let i = 0; i < sequence.length; i++) {
            if (sequence[i].number === number) return true;
        }
        return false;
    };
    this.setDist = function(number, dist) {
        for (let i = 0; i < sequence.length; i++) {
            if (sequence[i].number === number ){
                sequence[i].distance = dist;
                break;
            }
        }
        this.buildMinHeap();
    };
    this.showSequence = function () {
        console.log(sequence);
    };
    this.isEmpty = function () {
        return sequence.length === 0;
    };
    this.buildMinHeap = function () {
        for (let i = Math.floor(this.size() / 2); i >= 0; i--) {
            this.siftDown(i);
        }
        return sequence;
    };
    this.extractMin = function () {
        let result = sequence[0];
        sequence[0] = sequence[sequence.length - 1];
        sequence.pop();
        this.siftDown(0);
        return result;
    };
    this.siftDown = function (index) {
        minIndex = index;
        let l = this.leftChild(minIndex);
        let r = this.rightChild(minIndex);

        if (l <= this.size() && sequence[l].distance < sequence[minIndex].distance) {
            minIndex = l;
        }
        //console.log()
        if (r <= this.size() && sequence[r].distance < sequence[minIndex].distance) {
            minIndex = r;
        }
        if (index != minIndex) {
            this.swap(index, minIndex);
            this.siftDown(minIndex);
        }
    };
    this.swap = function (indexA, indexB) {
        let temporaryElement = sequence[indexA];
        sequence[indexA] = sequence[indexB];
        sequence[indexB] = temporaryElement;
        this.numbersOfSwaps++;
        this.swappedPairs.push([indexA, indexB]);
    };
    this.parent = function (i) {
        return Math.floor((i - 1) / 2);
    };
    this.leftChild = function (i) {
        return (2 * i) + 1;
    };
    this.rightChild = function (i) {
        return (2 * i) + 2;
    };
    let init = init => {
        this.buildMinHeap();
    };
    init();
}

function MinSpanningTree (N, coordinates) {
    let adjList = [];
    let nodes = createNodes();
    let init = () => {
        buildAdjList();
    };
    this.calculateTree = function () {
        let q = new PriorityQueue(createNodes());
        let dist = 0;
        while (!q.isEmpty()) {
            let current = q.extractMin();
            adjList[current.number].forEach(edge => {
                //console.log(`Current number: ${current.number}, EdgeDist ${edge.destination}`);
                //q.showSequence();
                if (q.isInQueue(edge.destination)) {
                    //console.log(edge)
                    if (nodes[edge.destination].distance > edge.cost) {
                        nodes[edge.destination].distance = edge.cost;
                        q.setDist(edge.destination, edge.cost)
                    }
                }
            });
            dist += current.distance;
        }
        console.log(dist)
    };
    function buildAdjList () {
        coordinates.forEach((el, i) => {
            adjList.push([]);
            for (let c = 0; c < coordinates.length; c++){
                if (c == i) continue;
                adjList[i].push(new Edge(coordinates[i], coordinates[c], i, c));
            }
        });
        //console.log(adjList);
    }
    function Edge (s, d, i, c){
        this.source = i;
        this.destination = c;
        this.cost = Math.sqrt(Math.pow((s[0] - d[0]), 2) + Math.pow((s[1] - d[1]), 2));
    }
    function createNodes (){
        let nodes = [];
        for (let i = 0; i < N; i++){
            nodes.push(new Node(i));
        }
        return nodes;
    }
    function Node(n) {
        this.number   = n;
        this.distance = n === 0 ? 0 :+Infinity;
        parent = false;
    }
    init();
}

function findDistance() {
    let t = new MinSpanningTree(n, list);
    t.calculateTree();
}

