/**
 * Created by HP on 10-Sep-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);

function helper(line) {
    intA = parseInt(line.split(' ').shift());
    intB = parseInt(line.split(' ').pop());
    console.log(leastCommonMultiplyNaive(intA, intB));
    rl.close();
    process.exit();
}

function leastCommonMultiplyNaive(intA, intB) {
    var lcm;

    for (var l = 1; l <= (intA * intB); l++){
        if (l % intA === 0 && l % intB === 0) {
            lcm = l;
            break;
        }
    }
    return lcm;
}