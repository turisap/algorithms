/**
 * Created by HP on 10-Sep-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', helper);

function helper(line) {
    console.log(greatestCommonDivisor(line));
    rl.close();
    process.exit();
}

function greatestCommonDivisor(line) {
    var intA = parseInt(line.split(' ').shift());
    var intB = parseInt(line.split(' ').pop());
    var currentGCD = 1;

    for (var i = 2; i <= intA && i <= intB; i++) {
        if ((intA % i == 0) && (intB % i == 0)) {
            currentGCD = i;
        }
    }
    return currentGCD;
}