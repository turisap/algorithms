/**
 * Created by HP on 16-Oct-17.
 */
function repeat( num) {
    return function() {
        //console.log(num)
        num++;
        return repeat(num)
    }
}

function trampoline(fn) {
    while(fn && typeof fn === 'function') {
        fn = fn()
    }
}

let test = function(num) {
    trampoline(function() {
        return repeat(num)
    })
}

test(1);