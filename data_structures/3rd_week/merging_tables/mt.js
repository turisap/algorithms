/**
 * Created by HP on 08-Oct-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', assigner);
var n = 0;
var m = 0;
var rows = [];
var merges = [];
var currentLine = 1;

function assigner(line) {
    if (currentLine === 1) {
        var data = line.trim().split(' ');
        n = parseInt(data.shift());
        m = parseInt(data.shift());
        //console.log(m)
    } else if (currentLine === 2) {
        rows = line.trim().split(' ').map(Number);
    } else if (currentLine > 2 && currentLine <= m + 1) {
        merges.push(line.trim().split(' ').map(Number));
    } else  {
        merges.push(line.trim().split(' ').map(Number));
        var results = mergeTables(rows, merges);
        results.forEach(function (item) {
            console.log(item);
        });
        rl.close();
        process.exit();
    }
    currentLine++;
}

function Table (number, rows) {
    this.parent = number;
    this.rows   = rows;
    this.rank   = 0;
}

function DisjointSet(tables, maxScore) {
    this.maxScore = maxScore;
    this.resultsOfMerging = [];
    this.find = function (i) {
        while (tables[i].parent != i) {
            i = tables[i].parent;
        }
        return tables[i];
    };
    this.merge = function (i, j) {
        if(i === j){this.resultsOfMerging.push(this.maxScore);return;}
        var iTable = this.find(i);
        var jTable = this.find(j);
        if (iTable.parent === jTable.parent){this.resultsOfMerging.push(this.maxScore);return;}
        if(iTable.rank > jTable.rank) {
            jTable.parent  = iTable.parent;
            iTable.rows += jTable.rows;
            jTable.rows  = 0;
            this.maxScore = iTable.rows > this.maxScore ? iTable.rows : this.maxScore;
        } else if (iTable.rank < jTable.rank){
            iTable.parent = jTable.parent;
            jTable.rows += iTable.rows;
            iTable.rows = 0;
            this.maxScore = jTable.rows > this.maxScore ? jTable.rows : this.maxScore;
        } else if (iTable.rank === jTable.rank) {
            iTable.parent = jTable.parent;
            jTable.rows += iTable.rows;
            iTable.rows = 0;
            jTable.rank += 1;
            this.maxScore = jTable.rows > this.maxScore ? jTable.rows : this.maxScore;
        }
        //console.log(tables);
        this.resultsOfMerging.push(this.maxScore);
    }
}

function mergeTables(rows, merges){
    var tables = [];
    var maxScore = 0;
    for (var i = 0, len = rows.length; i < len; i++) {
        if(rows[i] > maxScore){maxScore = rows[i]}
        tables.push(new Table(i, rows[i]));
    }
    var db = new DisjointSet(tables, maxScore);
    for (var c = 0, len2 = merges.length; c < len2; c++) {
        db.merge(merges[c][0] - 1, merges[c][1] - 1);
    }
     return db.resultsOfMerging;
}




     /*var test = new DisjointSet([new Table(0, 4), new Table(1, 3), new Table(2, 3), new Table(3, 5)]);
     test.merge(0, 1);
     test.merge(2, 0);
     test.merge(3, 2);*/