#python3
import sys
import math

def binary_search(a, x):
    left, right = 0, len(a) - 1
    
    while right >= left:
        guess = int(math.floor((left + right) / 2))
        #print(guess)
        if a[guess] == x:
            return guess
        elif a[guess] < x:
            left = guess + 1
        elif a[guess] > x:
            right = guess - 1
    return -1


if __name__ == '__main__':
   searchable = [0, 1, 2, 1155, 123454345]
   values     = [11553]
   for value in values:
        # replace with the call to binary_search when implemented
        print(binary_search(searchable, value))
