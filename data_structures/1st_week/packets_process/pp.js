/**
 * Created by HP on 04-Oct-17.
 */
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', assigner);
var currentLine = 1;
var bufferCapacity = 0;
var packetsNumber = 0;
var timeValues     = [];

function assigner (line) {
    if (currentLine === 1) {
        data = line.trim().split(' ');
        bufferCapacity = parseInt(data[0]);
        packetsNumber = parseInt(data[1]);
    }
    if (currentLine > 1) {
        timeValues.push(line.trim().split(' '));
    }
    if (currentLine == packetsNumber + 1) {
        var result = processor(timeValues, bufferCapacity);
        if (Array.isArray(result)) {
            result.forEach(function (res) {
                console.log(res);
            })
        } else {
            console.log(result)
        }
        rl.close();
        process.exit();
    }
    currentLine++;
}

function processor(values, bufferCapacity) {
    var resultingTime = [];

    this.init = function () {
        this.processor(this.reformatPackets(values));
    };

    this.processor = function (packages) {
        var buffer = [];
        var finishProcessing = 0;

        while (packages.length > 0 || buffer.length > 0) {
            if(bufferFirst()) {
                process(buffer.shift(), true);
                bufferCapacity++;
            } else if (packages.length > 0)  {
                var packetsToProcess = this.checkSeveralPacketsArriveSimulteniously(packages);
                process(packetsToProcess);
            }
        }
        function bufferFirst(){
            if (buffer.length > 0 && packages.length > 0) {return buffer[0].arrivalTime < packages[0].arrivalTime}
            if (buffer.length > 0 && packages.length === 0) {return true;}
        }

        /* cd xampp/htdocs/courses/algorithms/data_structures/1st_week/packets_process */

        function process(packetsToProcess, packFromBuffer = false) {
            //console.log(`finishProcessing = ${finishProcessing}`);
            if (Array.isArray(packetsToProcess)){
                //console.log('place1')
                packetsToProcess.forEach(function (packet) {
                    if(checkIfComputerCanAccept(packet)){
                        if (packFromBuffer) {
                            resultingTime.push(finishProcessing);
                            finishProcessing += packet.processingTime;
                        } else {
                            resultingTime.push(packet.arrivalTime);
                            finishProcessing += packet.processingTime;
                        }
                    } else {
                        sendPackToBufferOrDrop(packet);
                    }
                });
            } else {
                //console.log('place2')
                if(checkIfComputerCanAccept(packetsToProcess, packFromBuffer)){
                    if (packFromBuffer) {
                        resultingTime.push(finishProcessing);
                        finishProcessing += packetsToProcess.processingTime;
                    } else {
                        resultingTime.push(packet.arrivalTime);
                        finishProcessing += packetsToProcess.processingTime;
                    }
                } else {
                    sendPackToBufferOrDrop(packetsToProcess);
                }
            }

            function sendPackToBufferOrDrop(packet) {
                //console.log(`BufurCapacity = ${bufferCapacity}`);
                if (bufferCapacity > 0) {
                    buffer.push(packet);
                    bufferCapacity--;
                } else {
                    resultingTime.push(-1);
                }
            }
            function checkIfComputerCanAccept (pack, packFromBufer) {
                return (pack.arrivalTime >= finishProcessing) || packFromBufer;
            }
        }
    };

    this.checkSeveralPacketsArriveSimulteniously = function (packets) {
        var firstPacket = packets.shift();
        var packetsToProcess = [firstPacket];

        function packetsArriveAtSameTime() {
            if (packets.length > 0) {
                return firstPacket.arrivalTime === packets[0].arrivalTime;
            }
            return false;
        }

        while (packetsArriveAtSameTime()) {
            packetsToProcess.push(packets.shift());
        }
        return packetsToProcess;
    };
    this.reformatPackets = function (values) {
        var Package = function (valuePair) {
            this.arrivalTime = parseInt(valuePair.shift());
            this.processingTime = parseInt(valuePair.shift());
        };
        var packagesForProcessing = [];

        values.forEach(function(value) {
            packagesForProcessing.push(new Package(value));
        });
        return packagesForProcessing;
    };
    this.init();
    return resultingTime;
}